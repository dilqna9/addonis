package com.telerik.addonis.servicestests;

import com.telerik.addonis.Factory;
import com.telerik.addonis.models.IDE;
import com.telerik.addonis.repositories.IDERepository;
import com.telerik.addonis.services.IDEServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.ArgumentMatchers.anyString;

@RunWith(MockitoJUnitRunner.class)
public class IDEServiceImplTests {

    @Mock
    IDERepository mockIDERepository;

    @InjectMocks
    IDEServiceImpl ideService;

    @Test
    public void getAll_Should_Call_Repository(){

        ideService.getAll();

        Mockito.verify(mockIDERepository, Mockito.times(1)).findAll();

    }

    @Test
    public void getByNameShould_ReturnIDE_WhenIDEExist(){

        //Arrange
        IDE expectedIDE = Factory.createIDE();

        Mockito.when(mockIDERepository.findByName(anyString())).thenReturn(expectedIDE);

        //Act
        IDE returnIDE = ideService.getByName("name");

        //Assert
        Assert.assertSame(expectedIDE, returnIDE);

    }

}
