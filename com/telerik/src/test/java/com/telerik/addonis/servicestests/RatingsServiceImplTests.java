package com.telerik.addonis.servicestests;

import com.telerik.addonis.Factory;
import com.telerik.addonis.exceptions.EntityNotFoundException;
import com.telerik.addonis.models.Rating;
import com.telerik.addonis.repositories.RatingsRepository;
import com.telerik.addonis.services.RatingsServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.ArgumentMatchers.anyString;

@RunWith(MockitoJUnitRunner.class)
public class RatingsServiceImplTests {

    @Mock
    RatingsRepository mockRatingsRepository;

    @InjectMocks
    RatingsServiceImpl ratingsService;

    @Test
    public void deleteRating_Should_Call_Repository(){

        Rating rating = Factory.createRating();

        Mockito.when(mockRatingsRepository.existsById(anyString())).thenReturn(true);

        ratingsService.deleteById(rating.getId());
        Mockito.verify(mockRatingsRepository, Mockito.times(1)).deleteById(rating.getId());

    }

    @Test
    public void deleteRating_Should_Throw_WhenRatingNotExist(){

        Mockito.when(mockRatingsRepository.existsById(anyString())).thenReturn(false);
        //Act
        Assert.assertThrows(EntityNotFoundException.class,
                () -> ratingsService.deleteById(Factory.createAddon().getId()));

    }

    @Test
    public void createRating_Should_Call_Repository(){

        Rating rating = Factory.createRating();

        Mockito.when(mockRatingsRepository.existsByUser_IdAndAddon_Id(anyString(), anyString())).thenReturn(false);

        ratingsService.addRatingToAddon(rating);
        Mockito.verify(mockRatingsRepository, Mockito.times(1)).saveAndFlush(rating);

    }

    @Test
    public void createRating_Should_Call_Repository_WhenRatingAlreadyExist(){

        Rating rating = Factory.createRating();

        Mockito.when(mockRatingsRepository.existsByUser_IdAndAddon_Id(anyString(), anyString())).thenReturn(true);
        Mockito.when(mockRatingsRepository.findByUser_IdAndAddon_Id(anyString(), anyString())).thenReturn(rating);

        ratingsService.addRatingToAddon(rating);
        //Act
        Mockito.verify(mockRatingsRepository, Mockito.times(1)).saveAndFlush(rating);

    }

    @Test
    public void getByCreatorShould_ReturnRating_WhenRatingExist(){

        //Arrange
        Rating expectedRating = Factory.createRating();

        Mockito.when(mockRatingsRepository.existsByUser_Username(anyString())).thenReturn(true);
        Mockito.when(mockRatingsRepository.findByUser_Username(anyString())).thenReturn(expectedRating);

        //Act
        Rating returnRating = ratingsService.getRatingByCreator("username");

        //Assert
        Assert.assertSame(expectedRating, returnRating);

    }

    @Test
    public void getByCreatorShould_Throw_WhenRatingNotExist(){

        Mockito.when(mockRatingsRepository.existsByUser_Username(anyString())).thenReturn(false);
        //Act
        Assert.assertThrows(EntityNotFoundException.class,
                () -> ratingsService.getRatingByCreator("username"));

    }


    @Test
    public void getAvgRatingOfAddon_Should_Call_Repository(){

        ratingsService.getAvgRatingOfAddon(anyString());

        Mockito.verify(mockRatingsRepository, Mockito.times(1))
                .findAverageRatingOfAddon(anyString());

    }

}
