package com.telerik.addonis.servicestests;

import com.telerik.addonis.Factory;
import com.telerik.addonis.exceptions.EntityNotFoundException;
import com.telerik.addonis.models.GitHubData;
import com.telerik.addonis.repositories.GitHubDataRepository;
import com.telerik.addonis.services.GitHubDataServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.ArgumentMatchers.anyString;

@RunWith(MockitoJUnitRunner.class)
public class GitHubDataServiceImplTests {

    @Mock
    GitHubDataRepository mockGitHubDataRepository;

    @InjectMocks
    GitHubDataServiceImpl gitHubDataService;

    @Test
    public void createGitHubData_Should_Call_Repository(){

        GitHubData gitHubData = Factory.createGitHubData();

        gitHubDataService.createGitHubData(gitHubData);
        Mockito.verify(mockGitHubDataRepository, Mockito.times(1)).saveAndFlush(gitHubData);

    }

    @Test
    public void updateGitHubData_Should_Call_Repository(){

        GitHubData gitHubData = Factory.createGitHubData();

        gitHubDataService.updateGitHubData(gitHubData);
        Mockito.verify(mockGitHubDataRepository, Mockito.times(1)).saveAndFlush(gitHubData);

    }

}
