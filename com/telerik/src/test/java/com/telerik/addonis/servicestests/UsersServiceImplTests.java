package com.telerik.addonis.servicestests;

import com.telerik.addonis.Factory;
import com.telerik.addonis.exceptions.EntityAlreadyExistsException;
import com.telerik.addonis.exceptions.EntityNotFoundException;
import com.telerik.addonis.models.User;
import com.telerik.addonis.models.UserDetails;
import com.telerik.addonis.models.VerificationToken;
import com.telerik.addonis.repositories.AuthorityRepository;
import com.telerik.addonis.repositories.UserDetailsRepository;
import com.telerik.addonis.repositories.UsersRepository;
import com.telerik.addonis.repositories.VerificationTokenRepository;
import com.telerik.addonis.services.UsersServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.security.Principal;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;


@RunWith(MockitoJUnitRunner.class)
public class UsersServiceImplTests {

    @Mock
    UsersRepository mockUsersRepository;

    @Mock
    AuthorityRepository authorityRepository;

    @Mock
    VerificationTokenRepository tokenRepository;

    @Mock
    UserDetailsRepository mockUserDetailsRepository;


    @InjectMocks
    UsersServiceImpl usersService;

    @Test
    public void getByIdAndEnabledIsTrueShould_ReturnUser_WhenUserExist(){

        //Arrange
        User expectedUser = Factory.createUser();

        Mockito.when(mockUsersRepository.findByIdAndEnabledIsTrue(anyString())).thenReturn(expectedUser);
        Mockito.when(mockUsersRepository.existsByIdAndEnabledIsTrue(anyString())).thenReturn(true);
        //Act
        User returnUser = usersService.getUserByIdAndEnabledIsTrue("eb6c4aaf-b217-497c-b45a-de391d74dac7");

        //Assert
        Assert.assertSame(expectedUser, returnUser);

    }

    @Test
    public void getByIdAndEnabledIsTrueShould_Throw_WhenUserNotExist(){

        Mockito.when(mockUsersRepository.existsByIdAndEnabledIsTrue(anyString())).thenReturn(false);
        //Act
        Assert.assertThrows(EntityNotFoundException.class,
                () -> usersService.getUserByIdAndEnabledIsTrue("eb6c4aaf-b217-497c-b45a-de391d74dac7"));
    }


    @Test
    public void getByUsernameShould_ReturnUser_WhenUserExist(){

        //Arrange
        User expectedUser = Factory.createUser();

        Mockito.when(mockUsersRepository.findByUsernameAndEnabledIsTrue(anyString())).thenReturn(expectedUser);
        Mockito.when(mockUsersRepository.existsByUsernameAndEnabledIsTrue(anyString())).thenReturn(true);

        //Act
        User returnUser = usersService.getByUsernameAndEnabledIsTrue("username");

        //Assert
        Assert.assertSame(expectedUser, returnUser);

    }

    @Test
    public void getByUsernameShould_Throw_WhenUserNotExist(){

        Mockito.when(mockUsersRepository.existsByUsernameAndEnabledIsTrue(anyString())).thenReturn(false);
        //Act
        Assert.assertThrows(EntityNotFoundException.class,
                () -> usersService.getByUsernameAndEnabledIsTrue("username"));

    }

    @Test
    public void createUser_Should_Call_Repository(){

        User user = Factory.createUser();
        UserDetails userDetails = Factory.createUserDetails();
        usersService.createUser(user, userDetails);
        Mockito.verify(mockUsersRepository, Mockito.times(1)).saveAndFlush(user);

    }

    @Test
    public void createUser_Should_Throw_WhenUserAlreadyExist(){

        Mockito.when(mockUsersRepository.existsByUsername(anyString())).thenReturn(true);
        //Act
        Assert.assertThrows(EntityAlreadyExistsException.class,
                () -> usersService.createUser(Factory.createUser(), Factory.createUserDetails()));

    }


    @Test
    public void checkUserExists_Should_ReturnTrue_WhenUserExist(){

        Mockito.when(mockUsersRepository.existsByUsernameAndEnabledIsTrue(anyString())).thenReturn(true);
        //Act
        Assert.assertSame(usersService.checkUserExistsByUsernameAndEnabledIsTrue("username"), true);

    }

    @Test
    public void updateUser_Should_Call_Repository(){

        User user = Factory.createUser();
        UserDetails userDetails = user.getUserDetails();
        usersService.updateUser(user);
        Mockito.verify(mockUserDetailsRepository, Mockito.times(1)).saveAndFlush(userDetails);

    }

    @Test
    public void disableUser_Should_Call_Repository(){

        User user = Factory.createUser();
        UserDetails userDetails = user.getUserDetails();

        Mockito.when(mockUsersRepository.existsByIdAndEnabledIsTrue(anyString())).thenReturn(true);
        Mockito.when(mockUsersRepository.findByIdAndEnabledIsTrue(anyString())).thenReturn(user);

        usersService.disableUser(user.getId());

        Mockito.verify(mockUsersRepository, Mockito.times(1)).saveAndFlush(user);

    }

    @Test
    public void disableUserShould_Throw_WhenUserNotExist(){

        Mockito.when(mockUsersRepository.existsByIdAndEnabledIsTrue(anyString())).thenReturn(false);
        //Act
        Assert.assertThrows(EntityNotFoundException.class,
                () -> usersService.disableUser("id"));

    }

    @Test
    public void getAll_Should_Call_Repository(){

        usersService.getAllByEnabledIsTrue();

        Mockito.verify(mockUsersRepository, Mockito.times(1)).findAllByEnabledIsTrue();

    }

    @Test
    public void getAllByUsername_Should_Call_Repository(){

        usersService.getAllByUsernameAndEnabledIsTrue(anyString());

        Mockito.verify(mockUsersRepository, Mockito.times(1)).findAllByUsernameAndEnabledIsTrue(anyString());

    }

    @Test
    public void getByIdShould_ReturnUserDetails_WhenUserDetailsExist(){

        //Arrange
        UserDetails expectedUserDetails = Factory.createUserDetails();

        Mockito.when(mockUserDetailsRepository.findById(anyString())).thenReturn(java.util.Optional.of(expectedUserDetails));

        //Act
        UserDetails returnUserDetails = usersService.getUserDetailsById("eb6c4aaf-b217-497c-b45a-de391d74dac7");

        //Assert
        Assert.assertSame(expectedUserDetails, returnUserDetails);

    }

    @Test
    public void getByIdShould_Throw_WhenUserDetailsNotExist(){

        //Act
        Assert.assertThrows(EntityNotFoundException.class,
                () -> usersService.getUserDetailsById("eb6c4aaf-b217-497c-b45a-de391d74dac7"));

    }

    @Test
    public void getUserRole_Should_Return_Anonymous_WhenPrincipalIsNull() {

        //Act
        Principal principal = null;
        String role = usersService.getUserRole(principal,"dd");

        //Assert
        Assert.assertEquals(role,"ANONYMOUS");
    }

    @Test
    public void getByUsernameWhenIsEnabledIsFalseShould_ReturnUser_WhenUserExist(){

        //Arrange
        User expectedUser = Factory.createUser();

        Mockito.when(mockUsersRepository.findByUsernameAndEnabledIsFalse(anyString())).thenReturn(expectedUser);
        Mockito.when(mockUsersRepository.existsByUsernameAndEnabledIsFalse(anyString())).thenReturn(true);

        //Act
        User returnUser = usersService.getByUsernameAndEnabledIsFalse("username");

        //Assert
        Assert.assertSame(expectedUser, returnUser);

    }

    @Test
    public void getByUsernameWhenEnabledIsFalseShould_Throw_WhenUserNotExist(){

        Mockito.when(mockUsersRepository.existsByUsernameAndEnabledIsFalse(anyString())).thenReturn(false);
        //Act
        Assert.assertThrows(EntityNotFoundException.class,
                () -> usersService.getByUsernameAndEnabledIsFalse("username"));

    }

    @Test
    public void createToken_Should_Call_Repository(){
        User user = Factory.createUser();
        VerificationToken token = Factory.createToken();
        usersService.createVerificationToken(user);
        Mockito.verify(tokenRepository, Mockito.times(1)).saveAndFlush(any());

    }

    @Test
    public void getVerificationToken_Should_CallRepository(){
        VerificationToken expectedToken = Factory.createToken();

        Mockito.when(tokenRepository.findByToken(anyString())).thenReturn(expectedToken);

        VerificationToken returnToken = usersService.getVerificationToken(expectedToken.getToken());

        Assert.assertSame(expectedToken, returnToken);
    }


    @Test
    public void getAllByEnabledIsFalse_Should_Call_Repository(){

        usersService.getAllByEnabledIsFalse();

        Mockito.verify(mockUsersRepository, Mockito.times(1)).findAllByEnabledIsFalse();

    }

}
