package com.telerik.addonis.servicestests;


import com.telerik.addonis.Factory;
import com.telerik.addonis.exceptions.EntityAlreadyExistsException;
import com.telerik.addonis.exceptions.EntityNotFoundException;
import com.telerik.addonis.models.Addon;
import com.telerik.addonis.repositories.AddonsRepository;
import com.telerik.addonis.repositories.GitHubDataRepository;
import com.telerik.addonis.services.AddonsServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.ArrayList;

import static org.mockito.ArgumentMatchers.*;

@RunWith(MockitoJUnitRunner.class)
public class AddonsServiceImplTests {

    @Mock
    AddonsRepository mockAddonsRepository;

    @Mock
    GitHubDataRepository mockGitHubDataRepository;

    @InjectMocks
    AddonsServiceImpl addonsService;

    @Test
    public void getByIdShould_ReturnAddon_WhenAddonExist(){

        //Arrange
        Addon expectedAddon = Factory.createAddon();

        Mockito.when(mockAddonsRepository.findByIdAndEnabledIsTrue(anyString())).thenReturn(expectedAddon);
        Mockito.when(mockAddonsRepository.existsById(anyString())).thenReturn(true);
        //Act
        Addon returnAddon = addonsService.getById("eb6c4aaf-b217-497c-b45a-de391d74dac7");

        //Assert
        Assert.assertSame(expectedAddon, returnAddon);

    }

    @Test
    public void getByIdShould_Throw_WhenAddonNotExist(){

        Mockito.when(mockAddonsRepository.existsById(anyString())).thenReturn(false);
        //Act
        Assert.assertThrows(EntityNotFoundException.class,
                () -> addonsService.getById("eb6c4aaf-b217-497c-b45a-de391d74dac7"));

    }

    @Test
    public void getByNameShould_ReturnAddon_WhenAddonExist(){

        //Arrange
        Addon expectedAddon = Factory.createAddon();

        Mockito.when(mockAddonsRepository.findByName(anyString())).thenReturn(expectedAddon);
        Mockito.when(mockAddonsRepository.existsByNameAndEnabledIsTrue(anyString())).thenReturn(true);

        //Act
        Addon returnAddon = addonsService.getByName("name");

        //Assert
        Assert.assertSame(expectedAddon, returnAddon);

    }

    @Test
    public void getByNameShould_Throw_WhenAddonNotExist(){

        Mockito.when(mockAddonsRepository.existsByNameAndEnabledIsTrue(anyString())).thenReturn(false);
        //Act
        Assert.assertThrows(EntityNotFoundException.class,
                () -> addonsService.getByName("name"));

    }

    @Test
    public void createAddon_Should_Call_Repository(){

        Addon addon = Factory.createAddon();

        Mockito.when(mockAddonsRepository.existsByOriginLinkAndEnabledIsTrue(anyString())).thenReturn(false);
        Mockito.when(mockAddonsRepository.findByOriginLink(anyString())).thenReturn(Factory.createAddon());
        Mockito.when(mockAddonsRepository.existsByOriginLinkAndEnabledIsFalse(anyString())).thenReturn(true);

        addonsService.createAddon(addon, Factory.createGitHubData());
        Mockito.verify(mockAddonsRepository, Mockito.times(1)).saveAndFlush(addon);

    }

    @Test
    public void createAddon_Should_Throw_WhenAddonAlreadyExist(){

        Mockito.when(mockAddonsRepository.existsByOriginLinkAndEnabledIsTrue(anyString())).thenReturn(true);
//        Mockito.when(mockAddonsRepository.findByOriginLink(anyString())).thenReturn(Factory.createAddon());
//        Mockito.when(mockAddonsRepository.existsByOriginLinkAndEnabledIsFalse(anyString())).thenReturn(true);
        //Act
        Assert.assertThrows(EntityAlreadyExistsException.class,
                () -> addonsService.createAddon(Factory.createAddon(), Factory.createGitHubData()));
    }

    @Test
    public void updateAddon_Should_Call_Repository(){

        Addon addon = Factory.createAddon();

        Mockito.when(mockAddonsRepository.existsById(anyString())).thenReturn(true);

        addonsService.updateAddon(addon);
        Mockito.verify(mockAddonsRepository, Mockito.times(1)).saveAndFlush(addon);

    }

    @Test
    public void updateAddon_Should_Throw_WhenAddonNotExist(){

        Mockito.when(mockAddonsRepository.existsById(anyString())).thenReturn(false);
        //Act
        Assert.assertThrows(EntityNotFoundException.class,
                () -> addonsService.updateAddon(Factory.createAddon()));

    }

    @Test
    public void CheckExistsByOriginLink_Should_Call_Repository(){

        addonsService.checkAddonExistsByOriginLink(anyString());
        Mockito.verify(mockAddonsRepository, Mockito.times(1)).existsByOriginLinkAndEnabledIsTrue(anyString());

    }

    @Test
    public void getAll_Should_Call_Repository(){

        addonsService.getAll();

        Mockito.verify(mockAddonsRepository, Mockito.times(1)).findAllByEnabledIsTrueAndStatus_Name("approved");

    }

    @Test
    public void getAllByCreator_Should_Call_Repository(){

        addonsService.getAllByCreator(anyString());

        Mockito.verify(mockAddonsRepository, Mockito.times(1))
                .findAllByCreatedBy_UsernameAndEnabledIsTrue(anyString());

    }

    @Test
    public void getAllByStatus_Should_Call_Repository(){

        addonsService.getAllByStatus(anyString());

        Mockito.verify(mockAddonsRepository, Mockito.times(1))
                .findAllByStatus_Name(anyString());

    }

    @Test
    public void getAllByMultipleFilters_LastCommitDate_Should_Call_Repository(){

        addonsService.getAllByMultipleFilters("","","","", 1, 1);

        Mockito.verify(mockAddonsRepository, Mockito.times(1))
                .findAllByEnabledIsTrueAndStatus_NameAndNameLikeAndCreatedBy_UsernameLikeAndIde_NameLikeOrderByGitHubData_LastCommitDateDesc(anyString(),anyString(),anyString(),anyString(), any());

    }

    @Test
    public void getAllByMultipleFilters_UploadDate_Should_Call_Repository(){

        addonsService.getAllByMultipleFilters("","","","upload_date", 1, 1);

        Mockito.verify(mockAddonsRepository, Mockito.times(1))
                .findAllByEnabledIsTrueAndStatus_NameAndNameLikeAndCreatedBy_UsernameLikeAndIde_NameLikeOrderByUploadDateDesc(anyString(),anyString(),anyString(),anyString(), any());

    }

    @Test
    public void getAllByMultipleFilters_DownloadsCount_Should_Call_Repository(){

        addonsService.getAllByMultipleFilters("","","","downloads_count", 1, 1);

        Mockito.verify(mockAddonsRepository, Mockito.times(1))
                .findAllByEnabledIsTrueAndStatus_NameAndNameLikeAndCreatedBy_UsernameLikeAndIde_NameLikeOrderByDownloadsCountDesc(anyString(),anyString(),anyString(),anyString(), any());

    }


    @Test
    public void getAllByMultipleFilters_AddonName_Should_Call_Repository(){

        addonsService.getAllByMultipleFilters("","","","addon_name", 1, 1);

        Mockito.verify(mockAddonsRepository, Mockito.times(1))
                .findAllByEnabledIsTrueAndStatus_NameAndNameLikeAndCreatedBy_UsernameLikeAndIde_NameLikeOrderByName(anyString(),anyString(),anyString(),anyString(), any());

    }

    @Test
    public void getAllByPage_Should_Call_Repository(){

        addonsService.getAllByPage(1);
        Mockito.verify(mockAddonsRepository, Mockito.times(1)).findAllByEnabledIsTrueAndStatus_Name(anyString(), any());

    }

    @Test
    public void getAllByCreatorAndPage_Should_Call_Repository(){
        Page<Addon> page = new PageImpl<>(new ArrayList<>());

        Mockito.when(mockAddonsRepository.findAllByCreatedBy_UsernameAndEnabledIsTrue(anyString(), any())).thenReturn(page);
        addonsService.getAllByCreatorAndPage("", 1);

        Mockito.verify(mockAddonsRepository, Mockito.times(1)).findAllByCreatedBy_UsernameAndEnabledIsTrue(anyString(), any());

    }
}
