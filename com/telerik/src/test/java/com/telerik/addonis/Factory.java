package com.telerik.addonis;

import com.telerik.addonis.models.*;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class Factory {

    public static Addon createAddon(){
        Addon addon = new Addon();
        addon.setId("d3109a03-90c2-4dd7-9acb-6bb70fb9860f");
        addon.setName("Octotree");
        addon.setStatus(createStatus());
        addon.setCreatedBy(createUser());
        addon.setOriginLink("https://github.com/ovity/octotree");
        addon.setGitHubData(createGitHubData());
        addon.setDescription("ahahahah");
        addon.setDownloadsCount(5);
        addon.setFile(new byte[]{});
        addon.setDownloadLink("localhost:8080/download/pic.png");
        Set<Tag> tags = new HashSet<>();
        tags.add(createTag());
        addon.setTags(tags);
        return addon;
    }

    public static UserDetails createUserDetails(){
        UserDetails userDetails = new UserDetails();
        userDetails.setId("67069cb1-3a16-4793-8c60-c254551a335d");
        userDetails.setFirstName("Gosho");
        userDetails.setLastName("Ivanov");
        userDetails.setEmail("gosho@abv.bg");
        userDetails.setImage("pic.png");
        return userDetails;
    }

    public static User createUser(){
        User user = new User();
        user.setId("4c66f8e5-c18a-4778-ac9a-14174bc0a283");
        user.setUsername("Gosho");
        user.setPassword("pass");
        user.setEnabled(true);
        user.setUserDetails(createUserDetails());
        return user;
    }

    public static Status createStatus(){
        Status status = new Status();
        status.setId("f1d4c774-6483-4e7b-941a-f53918d9d0bb");
        status.setName("pending");
        return status;
    }

    public static Tag createTag(){
        Tag tag = new Tag();
        tag.setId("c68e630e-fbe4-42f0-9e1b-7011bfa644cb");
        tag.setName("java");
        return tag;
    }

    public static GitHubData createGitHubData(){
        GitHubData gitHubData = new GitHubData();
        gitHubData.setId("b4a7a3a7-6afc-456e-83f0-b4eb909e5564");
        gitHubData.setEnabled(true);
        gitHubData.setLastCommitTitle("commit");
        gitHubData.setLastCommitDate(new Date());
        gitHubData.setPullCount("50");
        gitHubData.setOpenIssuesCount("10");
        return gitHubData;
    }

    public static AddonUserList createAddonUserList(){
        AddonUserList addonUserList = new AddonUserList();
        addonUserList.setId("096d993f-0e7c-4bc5-86a1-8ff0ab20659f");
        addonUserList.setUser(createUser());
        addonUserList.setAddon(createAddon());
        addonUserList.setListType(createListType());
        return addonUserList;
    }

    public static ListType createListType(){
        ListType listType = new ListType();
        listType.setId("91879542-19d1-4131-b2a8-7ea0fee2cf91");
        listType.setType("admin");
        return listType;
    }

    public static IDE createIDE(){
        IDE ide = new IDE();
        ide.setId("6e5b02e3-17b3-414b-922e-6778ee68715c");
        ide.setName("eclipse");
        return ide;
    }

    public static Rating createRating(){
        Rating rating = new Rating();
        rating.setId("a4b7a56d-8293-43a6-b7f4-634efd759fc1");
        rating.setAddon(createAddon());
        rating.setUser(createUser());
        rating.setRating(6);
        return rating;
    }

    public static VerificationToken createToken(){
        VerificationToken token = new VerificationToken();

        token.setId("d32247d7-9fa9-4f17-bed1-7822371c473b");
        token.setToken("5012bc5b-b896-4ab5-85b1-f1139ab194bf");
        token.setUser(createUser());

        return token;
    }

}
