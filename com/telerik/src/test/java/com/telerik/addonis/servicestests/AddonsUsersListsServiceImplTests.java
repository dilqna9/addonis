package com.telerik.addonis.servicestests;

import com.telerik.addonis.Factory;
import com.telerik.addonis.exceptions.EntityAlreadyExistsException;
import com.telerik.addonis.exceptions.EntityNotFoundException;
import com.telerik.addonis.models.Addon;
import com.telerik.addonis.models.AddonUserList;
import com.telerik.addonis.models.ListType;
import com.telerik.addonis.repositories.AddonsUsersListsRepository;
import com.telerik.addonis.repositories.ListTypesRepository;
import com.telerik.addonis.services.AddonsUsersListsServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;

@RunWith(MockitoJUnitRunner.class)
public class AddonsUsersListsServiceImplTests {

    @Mock
    AddonsUsersListsRepository mockAddonsUsersListsRepository;

    @Mock
    ListTypesRepository mockListTypesRepository;


    @InjectMocks
    AddonsUsersListsServiceImpl addonsUsersListsService;



    @Test
    public void createAddonUserList_Should_Call_Repository(){

        AddonUserList addonUserList = Factory.createAddonUserList();

        Mockito.when(mockAddonsUsersListsRepository.existsByAddon_IdAndListType_Id(anyString(), anyString())).thenReturn(false);

        addonsUsersListsService.createAddonUserList(addonUserList);
        Mockito.verify(mockAddonsUsersListsRepository, Mockito.times(1)).saveAndFlush(addonUserList);

    }

    @Test
    public void createAddonUserList_Should_Throw_WhenAddonUserListAlreadyExist(){

        Mockito.when(mockAddonsUsersListsRepository.existsByAddon_IdAndListType_Id(anyString(), anyString())).thenReturn(true);
        //Act
        Assert.assertThrows(EntityAlreadyExistsException.class,
                () -> addonsUsersListsService.createAddonUserList(Factory.createAddonUserList()));

    }

    @Test
    public void deleteAddonUserList_Should_Call_Repository(){

        AddonUserList addonUserList = Factory.createAddonUserList();

        Mockito.when(mockAddonsUsersListsRepository.existsByAddon_IdAndListType_Id(anyString(), anyString())).thenReturn(true);

        addonsUsersListsService.deleteAddonUserList(addonUserList);
        Mockito.verify(mockAddonsUsersListsRepository, Mockito.times(1)).delete(addonUserList);

    }

    @Test
    public void deleteAddonUserList_Should_Throw_WhenAddonUserListNotExist(){

        Mockito.when(mockAddonsUsersListsRepository.existsByAddon_IdAndListType_Id(anyString(), anyString())).thenReturn(false);
        //Act
        Assert.assertThrows(EntityNotFoundException.class,
                () -> addonsUsersListsService.deleteAddonUserList(Factory.createAddonUserList()));

    }

    @Test
    public void getAddonUserListByAddon_IdAndListType_IdShould_ReturnAddon_WhenAddonUserListExist(){

        //Arrange
        AddonUserList expectedAddonUserList = Factory.createAddonUserList();

        Mockito.when(mockAddonsUsersListsRepository.existsByAddon_IdAndListType_Id(anyString(), anyString())).thenReturn(true);
        Mockito.when(mockAddonsUsersListsRepository.findByAddon_IdAndListType_Id(anyString(), anyString())).thenReturn(expectedAddonUserList);

        //Act
        AddonUserList returnAddonUserList = addonsUsersListsService.getByAddon_IdAndListType_Id(anyString(), anyString());

        //Assert
        Assert.assertSame(expectedAddonUserList, returnAddonUserList);

    }

    @Test
    public void getAddonUserListByAddonIdAndListTypeIdShould_Throw_WhenListTypeNotExist(){

        Mockito.when(mockAddonsUsersListsRepository.existsByAddon_IdAndListType_Id(anyString(), anyString())).thenReturn(false);
        //Act
        Assert.assertThrows(EntityNotFoundException.class,
                () -> addonsUsersListsService.getByAddon_IdAndListType_Id(anyString(), anyString()));

    }



    @Test
    public void getAddonsByListType_Should_ReturnAddon_WhenAddonUserListExist(){

        //Arrange
        List<Addon> expectedList = new ArrayList<>();

        Mockito.when(mockAddonsUsersListsRepository.findAllAddonsOfUserFromList(anyString())).thenReturn(expectedList);

        //Act
        List<Addon> returnAddonList = addonsUsersListsService.getAllAddonsByListType(anyString());

        //Assert
        Assert.assertSame(expectedList, returnAddonList);

    }

    @Test
    public void getAddonUserListByListTypeShould_ReturnListTypeOfAddon_WhenListTypeExist(){

        //Arrange
        ListType expectedListType = Factory.createListType();

        Mockito.when(mockListTypesRepository.existsByType(anyString())).thenReturn(true);
        Mockito.when(mockListTypesRepository.findByType(anyString())).thenReturn(expectedListType);

        //Act
        ListType returnListType = addonsUsersListsService.getByType(anyString());

        //Assert
        Assert.assertSame(expectedListType, returnListType);

    }

    @Test
    public void getByListTypeShould_Throw_WhenListTypeNotExist(){

        Mockito.when(mockListTypesRepository.existsByType(anyString())).thenReturn(false);
        //Act
        Assert.assertThrows(EntityNotFoundException.class,
                () -> addonsUsersListsService.getByType(anyString()));

    }

    @Test
    public void getAllAddonsByListType_Should_Call_Repository(){

        addonsUsersListsService.getAllAddonsByListType("", 1, 1);
        Mockito.verify(mockAddonsUsersListsRepository, Mockito.times(1)).findAllAddonsOfUserFromList(anyString(), any());

    }
}
