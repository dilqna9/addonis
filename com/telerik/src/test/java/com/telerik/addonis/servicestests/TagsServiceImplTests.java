package com.telerik.addonis.servicestests;

import com.telerik.addonis.Factory;
import com.telerik.addonis.exceptions.EntityAlreadyExistsException;
import com.telerik.addonis.exceptions.EntityNotFoundException;
import com.telerik.addonis.models.Tag;
import com.telerik.addonis.repositories.TagsRepository;
import com.telerik.addonis.services.TagsServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.ArgumentMatchers.anyString;

@RunWith(MockitoJUnitRunner.class)
public class TagsServiceImplTests {

    @Mock
    TagsRepository mockTagsRepository;

    @InjectMocks
    TagsServiceImpl tagsService;

    @Test
    public void createTag_Should_Call_Repository(){

        Tag tag = Factory.createTag();

        Mockito.when(mockTagsRepository.existsByName(anyString())).thenReturn(false);

        tagsService.createTag(tag);
        Mockito.verify(mockTagsRepository, Mockito.times(1)).saveAndFlush(tag);

    }

    @Test
    public void createTag_Should_Throw_WhenTagAlreadyExist(){

        Mockito.when(mockTagsRepository.existsByName(anyString())).thenReturn(true);
        //Act
        Assert.assertThrows(EntityAlreadyExistsException.class,
                () -> tagsService.createTag(Factory.createTag()));

    }

    @Test
    public void getByNameShould_ReturnTag_WhenTagExist(){

        //Arrange
        Tag expectedTag = Factory.createTag();

        Mockito.when(mockTagsRepository.findByName(anyString())).thenReturn(expectedTag);
        Mockito.when(mockTagsRepository.existsByName(anyString())).thenReturn(true);

        //Act
        Tag returnTag = tagsService.findByName("name");

        //Assert
        Assert.assertSame(expectedTag, returnTag);

    }

    @Test
    public void getByNameShould_Throw_WhenTagNotExist(){

        Mockito.when(mockTagsRepository.existsByName(anyString())).thenReturn(false);
        //Act
        Assert.assertThrows(EntityNotFoundException.class,
                () -> tagsService.findByName("name"));

    }


}
