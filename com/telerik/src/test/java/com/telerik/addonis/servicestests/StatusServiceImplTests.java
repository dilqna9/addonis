package com.telerik.addonis.servicestests;

import com.telerik.addonis.Factory;
import com.telerik.addonis.exceptions.EntityNotFoundException;
import com.telerik.addonis.models.Status;
import com.telerik.addonis.repositories.StatusRepository;
import com.telerik.addonis.services.StatusServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.ArgumentMatchers.anyString;

@RunWith(MockitoJUnitRunner.class)
public class StatusServiceImplTests {

    @Mock
    StatusRepository mockStatusRepository;

    @InjectMocks
    StatusServiceImpl statusService;

    @Test
    public void getByNameShould_ReturnStatus_WhenStatusExist(){

        //Arrange
        Status expectedStatus = Factory.createStatus();

        Mockito.when(mockStatusRepository.findByName(anyString())).thenReturn(expectedStatus);
        Mockito.when(mockStatusRepository.existsByName(anyString())).thenReturn(true);

        //Act
        Status returnStatus = statusService.getStatusByName("name");

        //Assert
        Assert.assertSame(expectedStatus, returnStatus);

    }

    @Test
    public void getByNameShould_Throw_WhenStatusNotExist(){

        Mockito.when(mockStatusRepository.existsByName(anyString())).thenReturn(false);
        //Act
        Assert.assertThrows(EntityNotFoundException.class,
                () -> statusService.getStatusByName("name"));

    }


}
