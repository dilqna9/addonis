package com.telerik.addonis.services.contracts;

import com.telerik.addonis.models.IDE;

import java.util.List;

public interface IDEService {

    List<IDE> getAll();

    IDE getByName(String ideName);
}
