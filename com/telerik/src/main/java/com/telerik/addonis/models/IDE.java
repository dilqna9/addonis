package com.telerik.addonis.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/*IDE table class containing the IDE name which is category for addons*/

@Getter
@Setter
@NoArgsConstructor
@Table(name = "ides")
@Entity
public class IDE {

    @Id
    @GeneratedValue(generator = "uuid-string")
    @GenericGenerator(name = "uuid-string", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "ide_id", nullable = false, unique = true, updatable = false)
    private String id;

    @Column(name = "ide_name")
    private String name;

}
