package com.telerik.addonis.services;

import com.telerik.addonis.exceptions.EntityAlreadyExistsException;
import com.telerik.addonis.exceptions.EntityNotFoundException;
import com.telerik.addonis.exceptions.MailSendException;
import com.telerik.addonis.models.Authority;
import com.telerik.addonis.models.User;
import com.telerik.addonis.models.UserDetails;
import com.telerik.addonis.models.VerificationToken;
import com.telerik.addonis.repositories.AuthorityRepository;
import com.telerik.addonis.repositories.UserDetailsRepository;
import com.telerik.addonis.repositories.UsersRepository;
import com.telerik.addonis.repositories.VerificationTokenRepository;
import com.telerik.addonis.services.contracts.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.List;
import java.util.UUID;

import static com.telerik.addonis.Constants.*;

/*service for users and user details creating implementation for create/update/get/delete users and users details
filter and sort users
and throwing exceptions where appropriate*/

@Service
public class UsersServiceImpl implements UsersService {


    private final UsersRepository usersRepository;
    private final UserDetailsRepository userDetailsRepository;
    private final AuthorityRepository authorityRepository;
    private final VerificationTokenRepository verificationTokenRepository;
    private final JavaMailSender javaMailSender;


    @Autowired
    public UsersServiceImpl(UsersRepository usersRepository,
                            UserDetailsRepository userDetailsRepository,
                            AuthorityRepository authorityRepository,
                            VerificationTokenRepository verificationTokenRepository,
                            JavaMailSender javaMailSender) {

        this.usersRepository = usersRepository;
        this.userDetailsRepository = userDetailsRepository;
        this.authorityRepository = authorityRepository;
        this.verificationTokenRepository = verificationTokenRepository;
        this.javaMailSender = javaMailSender;

    }

    @Override
    public boolean checkUserExistsByUsernameAndEnabledIsTrue(String username) {
        return usersRepository.existsByUsernameAndEnabledIsTrue(username);
    }

    @Override
    public void createUser(User user, UserDetails userDetails) {

        if (usersRepository.existsByUsername(user.getUsername())) {
            throw new EntityAlreadyExistsException(
                    String.format(ALREADY_EXITS_ERROR_MSG, "User", "username", user.getUsername()));
        }

        Authority authority = new Authority();
        authority.setUsername(user.getUsername());
        authority.setAuthority("ROLE_USER");
        authorityRepository.saveAndFlush(authority);
        userDetailsRepository.saveAndFlush(userDetails);
        usersRepository.saveAndFlush(user);
    }

    @Override
    public void updateUser(User user) {

        UserDetails userDetails = user.getUserDetails();

        usersRepository.saveAndFlush(user);
        userDetailsRepository.saveAndFlush(userDetails);

    }

    @Override
    public void disableUser(String userId) {
        if (!usersRepository.existsByIdAndEnabledIsTrue(userId)) {
            throw new EntityNotFoundException(
                    String.format(NOT_FOUND_ERROR_MSG, "User", "ID", userId));
        }

        User user = usersRepository.findByIdAndEnabledIsTrue(userId);
        user.setEnabled(false);
        usersRepository.saveAndFlush(user);

    }

    @Override
    public User getByUsernameAndEnabledIsTrue(String username) {

        if (!usersRepository.existsByUsernameAndEnabledIsTrue(username)) {
            throw new EntityNotFoundException(
                    String.format(NOT_FOUND_ERROR_MSG, "User", "username", username));
        }

        return usersRepository.findByUsernameAndEnabledIsTrue(username);
    }

    @Override
    public User getByUsernameAndEnabledIsFalse(String username) {
        if (!usersRepository.existsByUsernameAndEnabledIsFalse(username)) {
            throw new EntityNotFoundException(
                    String.format(NOT_FOUND_ERROR_MSG, "User", "username", username));
        }

        return usersRepository.findByUsernameAndEnabledIsFalse(username);
    }

    @Override
    public List<User> getAllByEnabledIsTrue() {
        return usersRepository.findAllByEnabledIsTrue();
    }

    @Override
    public List<User> getAllByEnabledIsFalse() {
        return usersRepository.findAllByEnabledIsFalse();
    }

    @Override
    public List<User> getAllByUsernameAndEnabledIsTrue(String username) {
        return usersRepository.findAllByUsernameAndEnabledIsTrue(username);
    }

    @Override
    public User getUserByIdAndEnabledIsTrue(String userId) {
        if (!usersRepository.existsByIdAndEnabledIsTrue(userId)) {
            throw new EntityNotFoundException(
                    String.format(NOT_FOUND_ERROR_MSG, "User", "ID", userId));
        }
        return usersRepository.findByIdAndEnabledIsTrue(userId);
    }

    @Override
    public User getById(String userId) {
        return usersRepository.findById(userId).orElseThrow(() -> new EntityNotFoundException(
                String.format(NOT_FOUND_ERROR_MSG, "User", "ID", userId)));
    }

    @Override
    public UserDetails getUserDetailsById(String userDetailsId) {

        return userDetailsRepository.findById(userDetailsId)
                .orElseThrow(() -> new EntityNotFoundException(
                        String.format(NOT_FOUND_ERROR_MSG, "User", "ID", userDetailsId)));

    }

    @Override
    public String getUserRole(Principal principal, String creatorUsername) {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String role = "";

        if (principal == null) {
            role = "ANONYMOUS";
        } else {
            boolean isAdmin = authentication.getAuthorities()
                    .stream()
                    .map(e -> e.getAuthority())
                    .anyMatch(e -> e.equals("ROLE_ADMIN"));
            if (isAdmin) {
                role = "ADMIN";
            } else {
                if (principal.getName().equals(creatorUsername)) {
                    role = "CREATOR";
                } else {
                    role = "USER";
                }
            }
        }
        return role;
    }

    @Override
    public VerificationToken createVerificationToken(User user) {
        VerificationToken token = new VerificationToken();
        token.setToken(UUID.randomUUID().toString());
        token.setUser(user);
        verificationTokenRepository.saveAndFlush(token);
        return token;
    }

    @Override
    public VerificationToken getVerificationToken(String verificationToken) {
        return verificationTokenRepository.findByToken(verificationToken);
    }

    @Override
    public void sendRegistrationNotification(User user, String token) {

        try {

            String url = "http://localhost:8080/users/registrationConfirm/" + user.getUsername() + "/" + token;

            SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
            simpleMailMessage.setTo(user.getUserDetails().getEmail());
            simpleMailMessage.setFrom("addonis.team3@gmail.com");
            simpleMailMessage.setSubject("Registration confirmation");
            simpleMailMessage.setText(String.format("Follow this link to activate your profile %s", url));
            javaMailSender.send(simpleMailMessage);
        }catch (MailException e){
            throw new MailSendException(e.getMessage());
        }
    }

}
