package com.telerik.addonis.controllers;

import com.telerik.addonis.models.Addon;
import com.telerik.addonis.models.User;
import com.telerik.addonis.services.contracts.AddonsService;
import com.telerik.addonis.services.contracts.AddonsUsersListsService;
import com.telerik.addonis.services.contracts.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/* MVC Controller displaying Addonis application home page and displaying selected addon lists on the home page*/

@Controller
@RequestMapping("/")
public class HomeController {

    private final AddonsUsersListsService addonsUsersListsService;
    private final AddonsService addonsService;

    @Autowired
    public HomeController(AddonsUsersListsService addonsUsersListsService, AddonsService addonsService) {
        this.addonsUsersListsService = addonsUsersListsService;
        this.addonsService = addonsService;
    }

    @GetMapping
    public String redirectToHomePage() {
        return "redirect:/newest?page=1";
    }


    @GetMapping("/access-denied")
    public String showErrorPage() {
        return "not-allowed";
    }

    @GetMapping("/newest")
    public String getNewest(Model model, @RequestParam int page) {


        model.addAttribute("tab", "newest");
        Page<Addon> addons = addonsService
                .getAllByMultipleFilters("%%", "%%", "%%", "upload_date", page, 4);
        model.addAttribute("addons", addons.getContent());


        model = supplyPageInformation(model, page, addons);
        return "index";
    }

    @GetMapping("/most-downloaded")
    public String getMostDownloaded(Model model, @RequestParam int page) {


        model.addAttribute("tab", "most-downloaded");
        Page<Addon> addons = addonsService
                .getAllByMultipleFilters("%%", "%%", "%%", "downloads_count", page, 4);
        model.addAttribute("addons", addons.getContent());

        model = supplyPageInformation(model, page, addons);
        return "index";
    }

    @GetMapping("/admin-addons")
    public String getAdminAddons(Model model, @RequestParam int page) {


        model.addAttribute("tab", "admin-addons");
        Page<Addon> addons = addonsUsersListsService.getAllAddonsByListType("admin", page, 4);
        model.addAttribute("addons", addons.getContent());

        model = supplyPageInformation(model, page, addons);
        return "index";
    }

    private Model supplyPageInformation(Model model, int page, Page<Addon> addons) {
        if (addons.hasPrevious()) {
            model.addAttribute("firstPage", false);
        }
        if (addons.hasNext()) {
            model.addAttribute("lastPage", false);
        }

        model.addAttribute("page", page);

        return model;
    }

}
