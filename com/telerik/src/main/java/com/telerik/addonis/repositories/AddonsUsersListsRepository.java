package com.telerik.addonis.repositories;

import com.telerik.addonis.models.Addon;
import com.telerik.addonis.models.AddonUserList;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AddonsUsersListsRepository extends JpaRepository<AddonUserList, String> {

    Optional<AddonUserList> findById(String addonUserListId);

    boolean existsByAddon_IdAndListType_Id(String addonId, String listId);

    AddonUserList findByAddon_IdAndListType_Id(String addonId, String listTypeId);

    List<AddonUserList> findAllByUser_IdAndListType_Id(String userId, String listId);

    @Query("select l.addon from AddonUserList l where l.listType.type = :#{#listType}")
    Page<Addon> findAllAddonsOfUserFromList(String listType, Pageable pageable);

    @Query("select l.addon from AddonUserList l where l.listType.type = :#{#listType}")
    List<Addon> findAllAddonsOfUserFromList(String listType);

}
