package com.telerik.addonis.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/*status table class containing status names*/

@Entity
@Table(name = "status")
@NoArgsConstructor
@Getter
@Setter
public class Status {

    @Id
    @GeneratedValue(generator = "uuid-string")
    @GenericGenerator(name = "uuid-string", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "status_id", nullable = false, unique = true, updatable = false)
    private String id;

    @Column(name = "status", nullable = false)
    private String name;
}
