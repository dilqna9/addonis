package com.telerik.addonis.controllers;

import com.telerik.addonis.exceptions.EntityAlreadyExistsException;
import com.telerik.addonis.exceptions.EntityNotFoundException;
import com.telerik.addonis.exceptions.MailSendException;
import com.telerik.addonis.models.Addon;
import com.telerik.addonis.models.User;
import com.telerik.addonis.models.UserDetails;
import com.telerik.addonis.models.VerificationToken;
import com.telerik.addonis.models.dto.UserDTO;
import com.telerik.addonis.models.dto.UserDetailsDTO;
import com.telerik.addonis.models.dto.UserPasswordDTO;
import com.telerik.addonis.services.contracts.AddonsService;
import com.telerik.addonis.services.contracts.UsersService;
import com.telerik.addonis.models.mappers.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.security.Principal;
import java.util.Base64;
import java.util.List;

import static com.telerik.addonis.Constants.CONFIRM_PASSWORD_ERROR_MSG;
import static com.telerik.addonis.Constants.WRONG_PASSWORD_ERROR_MSG;


/*MVC Controller containing implementation of registration and login of user
changing user details and change password
display user profile */

@Controller
@RequestMapping("/users")
public class UsersController {

    private final UsersService usersService;
    private final UserMapper userMapper;
    private final PasswordEncoder passwordEncoder;
    private final AddonsService addonsService;

    @Autowired
    public UsersController(UsersService usersService,
                           UserMapper userMapper,
                           PasswordEncoder passwordEncoder,
                           AddonsService addonsService) {
        this.usersService = usersService;
        this.userMapper = userMapper;
        this.passwordEncoder = passwordEncoder;
        this.addonsService = addonsService;
    }

    @ModelAttribute("user")
    public User supplyUser() {
        return new User();
    }

    @PostMapping("/register")
    public String register(@ModelAttribute @Valid UserDTO userDTO, Model model) {
        try {
            if (!userDTO.getPassword().equals(userDTO.getRetypePassword())) {
                model.addAttribute("errors", CONFIRM_PASSWORD_ERROR_MSG);
                return "register";
            }

            UserDetails userDetails = userMapper.mapUserDtoToUserDetails(userDTO);
            if(!userDTO.getImage().isEmpty()){
                userDetails.setImage(Base64.getEncoder().encodeToString(userDTO.getImage().getBytes()));
            }

            User user = userMapper.mapUserDtoToUser(userDTO, userDetails);

            usersService.createUser(user, userDetails);

            VerificationToken token = usersService.createVerificationToken(user);
            usersService.sendRegistrationNotification(user, token.getToken());

        } catch (EntityAlreadyExistsException | MailSendException | IOException e) {
            model.addAttribute("errors", e.getMessage());
            return "register";
        }
        return "redirect:/";
    }

    @GetMapping("/register")
    public String register(Model model) {
        model.addAttribute("userDTO", new UserDTO());
        return "register";
    }


    @GetMapping("/registrationConfirm/{username}/{token}")
    public String confirmRegistration(Model model,
                                      @PathVariable String token,
                                      @PathVariable String username) {

        VerificationToken verificationToken = usersService.getVerificationToken(token);
        if (verificationToken == null) {
            model.addAttribute("errors", "Registration Confirmation is invalid");
            return "registration-confirmation";
        }

        User user = usersService.getByUsernameAndEnabledIsFalse(username);

        user.setEnabled(true);
        usersService.updateUser(user);
        return "registration-confirmation";
    }


    @GetMapping("/profile")
    public String profile(Model model, Principal principal, @RequestParam int page) {
        User user = usersService.getByUsernameAndEnabledIsTrue(principal.getName());
        model.addAttribute("role", usersService.getUserRole(principal, user.getUsername()));
        model.addAttribute("user", user);

        Page<Addon> addons = addonsService.getAllByCreatorAndPage(principal.getName(), page);

        model.addAttribute("addons", addons.getContent());
        model = supplyPageInformation(model, page, addons);

        model.addAttribute("page", page);
        return "profile";
    }

    @ModelAttribute
    public UserPasswordDTO supplyUserPasswordDTO() {
        return new UserPasswordDTO();
    }

    @PostMapping("/password")
    public String changePassword(@ModelAttribute @Valid UserPasswordDTO userPasswordDto, Principal principal, Model model) {
        try {
            User user = usersService.getByUsernameAndEnabledIsTrue(principal.getName());

            if (!passwordEncoder.matches(userPasswordDto.getOldPassword(), user.getPassword())) {
                model.addAttribute("errors", WRONG_PASSWORD_ERROR_MSG);
                return "change-password";
            }

            if (!userPasswordDto.getPassword().equals(userPasswordDto.getPasswordConfirmation())) {
                model.addAttribute("errors", CONFIRM_PASSWORD_ERROR_MSG);
                return "change-password";
            }
            user.setPassword(passwordEncoder.encode(userPasswordDto.getPassword()));

            usersService.updateUser(user);
            return "redirect:/";

        } catch (EntityNotFoundException e) {
            model.addAttribute("errors", e.getMessage());
            return "change-password";
        }
    }

    @GetMapping("/password")
    public String changePassword(Model model) {
        return "change-password";
    }

    @PostMapping("/details")
    public String updateUserDetails(@ModelAttribute @Valid UserDetailsDTO userDetailsDTO, Principal principal, Model model) {
        try {
            User user = usersService.getByUsernameAndEnabledIsTrue(principal.getName());
            userMapper.mapUserDetailsDtoToUserDetails(userDetailsDTO, principal.getName());
            if (!userDetailsDTO.getImage().isEmpty()) {
                user.getUserDetails().setImage(Base64.getEncoder().encodeToString(userDetailsDTO.getImage().getBytes()));
            }

            if(userDetailsDTO.getEnabled() != null) {
                user.setEnabled(userDetailsDTO.getEnabled());
            }

            usersService.updateUser(user);

            return "redirect:/";
        }catch (EntityNotFoundException | IOException e){
            model.addAttribute("errors", e.getMessage());
            return "not-allowed";
        }
    }

    @GetMapping("/details")
    public String updateUserDetails(Model model, Principal principal) {
        try {
            User user = usersService.getByUsernameAndEnabledIsTrue(principal.getName());

            UserDetailsDTO userDetailsDTO = userMapper.mapUserDetailsToUserDetailsDTO(user);
            String role = usersService.getUserRole(principal, "");
            model.addAttribute("role", role);
            model.addAttribute("userDetailsDTO", userDetailsDTO);
            return "update-user-details";
        }catch (EntityNotFoundException e){
            model.addAttribute("errors", e.getMessage());
            return "not-allowed";
        }
    }

    private Model supplyPageInformation(Model model, int page, Page<Addon> addons) {
        if (addons.hasPrevious()) {
            model.addAttribute("firstPage", false);
        }
        if (addons.hasNext()) {
            model.addAttribute("lastPage", false);
        }

        model.addAttribute("page", page);

        return model;
    }
}
