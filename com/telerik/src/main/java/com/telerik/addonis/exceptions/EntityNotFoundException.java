package com.telerik.addonis.exceptions;

/*
Custom Exception for not found object in the database
 */

public class EntityNotFoundException extends RuntimeException {

    public EntityNotFoundException(String message) {
        super(message);
    }
}

