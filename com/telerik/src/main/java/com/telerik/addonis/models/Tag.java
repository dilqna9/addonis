package com.telerik.addonis.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.Size;

/*tag table class containing tag names existing in the database*/

@Entity
@Table(name = "tags")
@NoArgsConstructor
@Getter
@Setter
public class Tag {

    @Id
    @GeneratedValue(generator = "uuid-string")
    @GenericGenerator(name = "uuid-string", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "tag_id", nullable = false, unique = true, updatable = false)
    private String id;

    @Column(name = "name", nullable = false)
    @Size(min = 1, max = 30, message = "Name of tag must be between 1 and 30 characters")
    private String name;
}
