package com.telerik.addonis.models.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/*DTO for updating user password*/

@NoArgsConstructor
@Getter
@Setter
public class UserPasswordDTO {

    private String oldPassword;

    @Size(min = 6, max = 68, message = "Password must be between 6 and 20 characters")
    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{6,}$")
    private String password;

    private String passwordConfirmation;
}
