package com.telerik.addonis.repositories;

import com.telerik.addonis.models.Addon;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


import java.util.List;

/*
repository for addons allowing functionalities for create/update/get/delete addon
filter and sort addons
boolean checks for existing addon
 */


@Repository
public interface AddonsRepository extends JpaRepository<Addon, String> {

    boolean existsById(String addonId);

    boolean existsByNameAndEnabledIsTrue(String addonName);

    boolean existsByOriginLinkAndEnabledIsTrue(String addonOriginLink);

    boolean existsByOriginLinkAndEnabledIsFalse(String addonOriginLink);

    Page<Addon> findAllByEnabledIsTrueAndStatus_Name(String status_name, Pageable pageable);

    List<Addon> findAllByEnabledIsTrueAndStatus_Name(String status_name);

    Addon findByIdAndEnabledIsTrue(String addonId);

    Addon findByName(String addonName);

    Addon findByOriginLink(String originLink);

    Page<Addon> findAllByCreatedBy_UsernameAndEnabledIsTrue(String creatorUsername, Pageable pageable);

    List<Addon> findAllByCreatedBy_UsernameAndEnabledIsTrue(String creatorUsername);

    List<Addon> findAllByStatus_Name(String statusName);

    Page<Addon> findAllByEnabledIsTrueAndStatus_NameAndNameLikeAndCreatedBy_UsernameLikeAndIde_NameLikeOrderByName(String statusName, String addonName, String creatorUsername, String ideName, Pageable pageable);

    Page<Addon> findAllByEnabledIsTrueAndStatus_NameAndNameLikeAndCreatedBy_UsernameLikeAndIde_NameLikeOrderByDownloadsCountDesc(String statusName, String addonName, String creatorUsername, String ideName , Pageable pageable);

    Page<Addon> findAllByEnabledIsTrueAndStatus_NameAndNameLikeAndCreatedBy_UsernameLikeAndIde_NameLikeOrderByUploadDateDesc(String statusName, String addonName, String creatorUsername, String ideName, Pageable pageable);

    Page<Addon> findAllByEnabledIsTrueAndStatus_NameAndNameLikeAndCreatedBy_UsernameLikeAndIde_NameLikeOrderByGitHubData_LastCommitDateDesc(String statusName, String addonName, String creatorUsername, String ideName, Pageable pageable);

}
