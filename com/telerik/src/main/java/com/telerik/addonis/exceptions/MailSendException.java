package com.telerik.addonis.exceptions;

/*
Custom Exception for send mail for registration users
 */

public class MailSendException extends RuntimeException {

    public MailSendException(String message) {
        super(message);
    }
}
