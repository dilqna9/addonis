package com.telerik.addonis.models.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/*DTO for filtering and sorting addons*/

@Setter
@Getter
@NoArgsConstructor
public class FilterSortDTO {

    private String addonName = "";
    private String creatorUsername = "";
    private String ideName = "";
    private String orderBy = "";
    private String emptyString = "";

    public static String order(String orderBy) {
        return orderBy;
    }
}
