package com.telerik.addonis.controllers.rest;

import com.telerik.addonis.exceptions.EntityNotFoundException;
import com.telerik.addonis.models.Addon;
import com.telerik.addonis.services.contracts.AddonsService;
import com.telerik.addonis.services.contracts.GitHubDataService;
import com.telerik.addonis.models.mappers.AddonMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;

/*Rest Controller containing implementation of updating git hub data */

@RestController
@RequestMapping("api/github")
public class GitHubDataRestController {

    private final AddonMapper addonMapper;
    private final AddonsService addonsService;
    private final GitHubDataService gitHubDataService;

    @Autowired
    public GitHubDataRestController(AddonMapper addonMapper, AddonsService addonsService, GitHubDataService gitHubDataService) {
        this.addonMapper = addonMapper;
        this.addonsService = addonsService;
        this.gitHubDataService = gitHubDataService;
    }

    @PutMapping("/update/{addonId}")
    public Addon updateGitHubDataNow(@PathVariable String addonId) {
        try {
            Addon addon = addonsService.getById(addonId);
            gitHubDataService.updateGitHubData(addonMapper.updateGitHubData(addon));

            return addon;
        }catch (EntityNotFoundException | IOException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
