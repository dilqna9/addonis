package com.telerik.addonis.repositories;

import com.telerik.addonis.models.GitHubData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/*repository for github data allowing functionalities for create/get github data
boolean check for existing github data object*/

@Repository
public interface GitHubDataRepository extends JpaRepository<GitHubData, String> {

    boolean existsById(String gitHubDataId);

}
