package com.telerik.addonis.services;

import com.telerik.addonis.exceptions.EntityAlreadyExistsException;
import com.telerik.addonis.exceptions.EntityNotFoundException;
import com.telerik.addonis.models.Tag;
import com.telerik.addonis.repositories.TagsRepository;
import com.telerik.addonis.services.contracts.TagsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.telerik.addonis.Constants.ALREADY_EXITS_ERROR_MSG;
import static com.telerik.addonis.Constants.NOT_FOUND_ERROR_MSG;

/*service for tags creating implementation for create/get tag
and throwing exceptions where appropriate*/

@Service
public class TagsServiceImpl implements TagsService {

    private final TagsRepository tagsRepository;

    @Autowired
    public TagsServiceImpl(TagsRepository tagsRepository) {
        this.tagsRepository = tagsRepository;
    }

    @Override
    public void createTag(Tag tag) {
        if(tagsRepository.existsByName(tag.getName().toLowerCase())){
            throw new EntityAlreadyExistsException(
                    String.format(ALREADY_EXITS_ERROR_MSG, "Tag", "name", tag.getName()));
        }
        tagsRepository.saveAndFlush(tag);
    }

    @Override
    public Tag findByName(String tagName) {
        if (!tagsRepository.existsByName(tagName)) {
            throw new EntityNotFoundException(
                    String.format(NOT_FOUND_ERROR_MSG, "Tag", "name", tagName));
        }
        return tagsRepository.findByName(tagName);
    }

}
