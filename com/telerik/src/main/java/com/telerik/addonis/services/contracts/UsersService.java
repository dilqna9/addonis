package com.telerik.addonis.services.contracts;

import com.telerik.addonis.models.User;
import com.telerik.addonis.models.UserDetails;
import com.telerik.addonis.models.VerificationToken;

import java.security.Principal;
import java.util.List;
import java.util.Optional;

public interface UsersService {

    boolean checkUserExistsByUsernameAndEnabledIsTrue(String username);

    void createUser(User user, UserDetails userDetails);

    void updateUser(User user);

    void disableUser(String userId);

    User getByUsernameAndEnabledIsTrue(String username);

    User getByUsernameAndEnabledIsFalse(String username);

    List<User> getAllByEnabledIsTrue();

    List<User> getAllByEnabledIsFalse();

    List<User> getAllByUsernameAndEnabledIsTrue(String username);

    User getUserByIdAndEnabledIsTrue(String userId);

    User getById(String userId);

    UserDetails getUserDetailsById(String userDetailsId);

    String getUserRole(Principal principal, String creatorUsername);

    VerificationToken createVerificationToken(User user);

    VerificationToken getVerificationToken(String VerificationToken);

    void sendRegistrationNotification(User user, String token);

}
