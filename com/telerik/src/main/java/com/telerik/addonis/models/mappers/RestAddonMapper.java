package com.telerik.addonis.models.mappers;

import com.telerik.addonis.models.Addon;
import com.telerik.addonis.models.GitHubData;
import com.telerik.addonis.models.dto.RestAddonDTO;
import com.telerik.addonis.services.contracts.IDEService;
import com.telerik.addonis.services.contracts.StatusService;
import com.telerik.addonis.services.contracts.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.Date;

/*mapper for create and update functionality of addons through rest service*/

@Component
public class RestAddonMapper {

    private final IDEService ideService;
    private final StatusService statusService;
    private final UsersService usersService;

    @Autowired
    public RestAddonMapper(IDEService ideService, StatusService statusService, UsersService usersService) {
        this.ideService = ideService;
        this.statusService = statusService;
        this.usersService = usersService;
    }

    public Addon mapAddonDTOtoAddon(RestAddonDTO restAddonDTO, String username, GitHubData gitHubData) {

        Addon addon = new Addon();
        addon.setName(restAddonDTO.getName());
        addon.setDescription(restAddonDTO.getDescription());
        addon.setOriginLink(restAddonDTO.getOriginLink());

        addon.setIde(ideService.getByName(restAddonDTO.getIdeName()));

        addon.setUploadDate(new Date());

        addon.setEnabled(true);
        addon.setStatus(statusService.getStatusByName("pending"));
        addon.setCreatedBy(usersService.getByUsernameAndEnabledIsTrue(username));
        addon.setGitHubData(gitHubData);

        return addon;
    }

    public Addon setDownloadLinkToAddon(Addon addon, MultipartFile file) {
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/download/")
                .path(addon.getId())
                .path("/")
                .path(fileName)
                .toUriString();
        addon.setDownloadLink(fileDownloadUri);
        return addon;
    }
}
