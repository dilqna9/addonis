package com.telerik.addonis.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;


/*git hub data table class containing the data from git hub for every addon*/

@Entity
@Table(name = "github_data")
@NoArgsConstructor
@Getter
@Setter
public class GitHubData {

    @Id
    @GeneratedValue(generator = "uuid-string")
    @GenericGenerator(name = "uuid-string", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "github_data_id", nullable = false, unique = true, updatable = false)
    private String id;

    @Column(name = "open_issues_count")
    private String openIssuesCount;

    @Column(name = "pulls_count")
    private String pullCount;

    @Column(name = "last_commit_title")
    private String lastCommitTitle;

    @Column(name = "last_commit_date")
    private Date lastCommitDate;

    @Column(name = "enabled", nullable = false)
    private boolean enabled;
}
