package com.telerik.addonis.repositories;

import com.telerik.addonis.models.Authority;
import org.springframework.data.jpa.repository.JpaRepository;

/*repository for authorities allowing functionalities for creating authority*/


public interface AuthorityRepository extends JpaRepository<Authority, String> {

}
