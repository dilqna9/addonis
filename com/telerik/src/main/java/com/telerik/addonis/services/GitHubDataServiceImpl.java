package com.telerik.addonis.services;

import com.telerik.addonis.models.GitHubData;
import com.telerik.addonis.repositories.GitHubDataRepository;
import com.telerik.addonis.services.contracts.GitHubDataService;
import org.kohsuke.github.GHIssueState;
import org.kohsuke.github.GitHub;
import org.kohsuke.github.GitHubBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Date;

import static com.telerik.addonis.Constants.*;


/*service for git hub data creating implementation for create/update/get git hub data
and throwing exceptions where appropriate*/

@Service
public class GitHubDataServiceImpl implements GitHubDataService {

    private final GitHubDataRepository gitHubDataRepository;

    @Autowired
    public GitHubDataServiceImpl(GitHubDataRepository gitHubDataRepository) {
        this.gitHubDataRepository = gitHubDataRepository;
    }

    public boolean validateRepoUrl(String repo){
        if(!repo.startsWith(GITHUB_URL_PREFIX)){
            return false;
        }
        repo = repo.substring(GITHUB_URL_PREFIX.length());
        String[] words = repo.split("/");
        return words.length >= 2;
    }

    @Override
    public GitHub getGitHubConnection() throws IOException {
        GitHub gitHub = new GitHubBuilder()
                .withOAuthToken(GITHUB_TOKEN)
                .build();
        return gitHub;
    }

    @Override
    public String getIssuesCount(String url) throws IOException {

        GitHub gitHubConnection = getGitHubConnection();
        String issuesCount = String.valueOf(gitHubConnection.getRepository(url).listIssues(GHIssueState.OPEN).toList().size());

        return issuesCount;
    }

    @Override
    public String getPullsCount(String url) throws IOException {

        GitHub githubConnection = getGitHubConnection();
        String pullsCount = String.valueOf(githubConnection.getRepository(url).getPullRequests(GHIssueState.ALL).size());

        return pullsCount;
    }

    @Override
    public Date getCommitDate(String url) throws IOException {

        GitHub githubConnection = getGitHubConnection();
        Date commitDate = githubConnection.getRepository(url).listCommits().toList().get(0).getCommitShortInfo().getCommitDate();

        return commitDate;
    }

    public String getCommitTitle(String url) throws IOException {

        GitHub gitHubConnection = getGitHubConnection();
        String commitTitle = gitHubConnection.getRepository(url).listCommits().toList().get(0).getCommitShortInfo().getMessage();

        return commitTitle;
    }

    @Override
    public void createGitHubData(GitHubData gitHubData) {
        gitHubDataRepository.saveAndFlush(gitHubData);
    }

    @Override
    public void updateGitHubData(GitHubData gitHubData) {
        gitHubDataRepository.saveAndFlush(gitHubData);
    }

}
