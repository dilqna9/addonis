package com.telerik.addonis.exceptions;

/*
Custom Exception for wrong git hub url
 */

public class WrongGitHubRepoException extends RuntimeException{

    public WrongGitHubRepoException(String message){
        super(message);
    }
}
