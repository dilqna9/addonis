package com.telerik.addonis.models.mappers;

import com.telerik.addonis.exceptions.WrongGitHubRepoException;
import com.telerik.addonis.models.Addon;
import com.telerik.addonis.models.GitHubData;
import com.telerik.addonis.models.dto.AddonDTO;
import com.telerik.addonis.services.contracts.GitHubDataService;
import com.telerik.addonis.services.contracts.IDEService;
import com.telerik.addonis.services.contracts.StatusService;
import com.telerik.addonis.services.contracts.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.IOException;
import java.util.Base64;
import java.util.Date;

import static com.telerik.addonis.Constants.GITHUB_URL_ERROR_MSG;
import static com.telerik.addonis.Constants.GITHUB_URL_PREFIX;

/*mapper for create and update functionality of addons*/


@Component
public class AddonMapper {

    private final StatusService statusService;
    private final UsersService usersService;
    private final GitHubDataService gitHubDataService;
    private final IDEService ideService;

    @Autowired
    public AddonMapper(StatusService statusService,
                       UsersService usersService,
                       GitHubDataService gitHubDataService,
                       IDEService ideService) {
        this.statusService = statusService;
        this.usersService = usersService;
        this.gitHubDataService = gitHubDataService;
        this.ideService = ideService;
    }

    public Addon mapAddonDTOtoAddon(AddonDTO addonDTO, String username, GitHubData gitHubData) throws IOException {

        Addon addon = new Addon();
        addon.setName(addonDTO.getName());
        addon.setDescription(addonDTO.getDescription());
        addon.setOriginLink(addonDTO.getOriginLink());

        addon.setIde(ideService.getByName(addonDTO.getIdeName()));

        addon.setFile(Base64.getEncoder().encode(addonDTO.getFile().getBytes()));

        addon.setUploadDate(new Date());

        addon.setEnabled(true);
        addon.setStatus(statusService.getStatusByName("pending"));
        addon.setCreatedBy(usersService.getByUsernameAndEnabledIsTrue(username));
        addon.setGitHubData(gitHubData);
        return addon;
    }

    public Addon mapAddonDTOtoAddon(AddonDTO addonDTO, Addon addon) {

        addon.setName(addonDTO.getName());
        addon.setDescription(addonDTO.getDescription());

        return addon;
    }

    public Addon setDownloadLinkToAddon(Addon addon, AddonDTO addonDTO) {
        String fileName = StringUtils.cleanPath(addonDTO.getFile().getOriginalFilename());
        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/download/")
                .path(addon.getId())
                .path("/")
                .path(fileName)
                .toUriString();
        addon.setDownloadLink(fileDownloadUri);
        return addon;
    }

    public GitHubData findGitHubData(String originLink) throws IOException {
        GitHubData gitHubData = new GitHubData();

        if (!gitHubDataService.validateRepoUrl(originLink)) {
            throw new WrongGitHubRepoException(String.format(GITHUB_URL_ERROR_MSG, originLink));
        }

        String url = originLink.substring(GITHUB_URL_PREFIX.length());
        gitHubData.setOpenIssuesCount(gitHubDataService.getIssuesCount(url));
        gitHubData.setPullCount(gitHubDataService.getPullsCount(url));
        gitHubData.setLastCommitDate(gitHubDataService.getCommitDate(url));
        gitHubData.setLastCommitTitle(gitHubDataService.getCommitTitle(url));

        gitHubData.setEnabled(true);

        return gitHubData;
    }

    public GitHubData updateGitHubData(Addon addon) throws IOException {
        GitHubData gitHubData = addon.getGitHubData();

        String url = addon.getOriginLink().substring(GITHUB_URL_PREFIX.length());
        gitHubData.setOpenIssuesCount(gitHubDataService.getIssuesCount(url));
        gitHubData.setPullCount(gitHubDataService.getPullsCount(url));
        gitHubData.setLastCommitDate(gitHubDataService.getCommitDate(url));
        gitHubData.setLastCommitTitle(gitHubDataService.getCommitTitle(url));

        return gitHubData;
    }


    public AddonDTO mapAddonToAddonDTO(Addon addon) {

        AddonDTO addonDTO = new AddonDTO();
        addonDTO.setName(addon.getName());
        addonDTO.setDescription(addon.getDescription());
        addonDTO.setOriginLink(addon.getOriginLink());
        addonDTO.setIdeName(addon.getIde().getName());

        return addonDTO;
    }
}
