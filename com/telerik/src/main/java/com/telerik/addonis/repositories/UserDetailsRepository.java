package com.telerik.addonis.repositories;

import com.telerik.addonis.models.UserDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/*repository for user details allowing functionalities for create/update/get user details
boolean check for existing user details*/

@Repository
public interface UserDetailsRepository  extends JpaRepository<UserDetails, String> {

    boolean existsById(String userDetailsId);

    void deleteById(String userDetailsId);

    Optional<UserDetails> findById(String userDetailsId);
}
