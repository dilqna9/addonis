package com.telerik.addonis.controllers;

import com.telerik.addonis.models.Addon;
import com.telerik.addonis.models.User;
import com.telerik.addonis.services.contracts.AddonsService;
import com.telerik.addonis.services.contracts.StatusService;
import com.telerik.addonis.services.contracts.UsersService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

import static com.telerik.addonis.Constants.NOT_ALLOWED;

/*MVC Controller containing implementation of approving addons, enabling/disabling users
and viewing list of all users */

@Controller
@RequestMapping("/admin")
public class AdminController {

    private final UsersService usersService;
    private final AddonsService addonsService;
    private final StatusService statusService;

    public AdminController(UsersService usersService,
                           AddonsService addonsService,
                           StatusService statusService) {
        this.usersService = usersService;
        this.addonsService = addonsService;
        this.statusService = statusService;
    }

    @PostMapping("/{addonId}/approve")
    public String approveAddon(@PathVariable String addonId, Model model, Principal principal) {

        if (!usersService.getUserRole(principal, "").equals("ADMIN")) {
            model.addAttribute("errors", NOT_ALLOWED);
            return "not-allowed";
        }

        Addon addon = addonsService.getById(addonId);

        addon.setStatus(statusService.getStatusByName("approved"));

        addonsService.updateAddon(addon);

        return "redirect:/addons/filter/1?addonName=&creatorUsername=&ideName=&orderBy=addon_name";
    }

    @GetMapping("/users/enabled")
    public String browseEnabledUsers(Model model) {
        List<User> users = usersService.getAllByEnabledIsTrue();
        model.addAttribute("users", users);

        return "users";
    }

    @GetMapping("/users/disabled")
    public String browseDisabledUsers(Model model) {
        List<User> users = usersService.getAllByEnabledIsFalse();
        model.addAttribute("users", users);

        return "users";
    }


    @GetMapping("/users/{userId}")
    public String profile(@PathVariable String userId, Model model, @RequestParam int page) {
        User user = usersService.getById(userId);
        model.addAttribute("user", user);
        model.addAttribute("role", "ADMIN");

        Page<Addon> addons = addonsService.getAllByCreatorAndPage(user.getUsername(), page);
        model.addAttribute("addons", addons.getContent());
        model = supplyPageInformation(model, page, addons);

        model.addAttribute("page", page);

        return "admin-user-profile";
    }

    @PostMapping("/users/{userId}/delete")
    public String deleteUser(@PathVariable String userId) {
        User user = usersService.getUserByIdAndEnabledIsTrue(userId);

        user.setEnabled(false);
        usersService.updateUser(user);

        return "redirect:/admin/users/disabled";
    }

    @PostMapping("/users/{userId}/enable")
    public String enableUser(@PathVariable String userId) {
        User user = usersService.getById(userId);

        user.setEnabled(true);
        usersService.updateUser(user);

        return "redirect:/admin/users/enabled";
    }

    @GetMapping("/addons/pending")
    public String pendingAddons(Model model){
        List<Addon> addons = addonsService.getAllPendingAddons();
        model.addAttribute("addons", addons);
        return "addons-pending";
    }

    private Model supplyPageInformation(Model model, int page, Page<Addon> addons) {
        if (addons.hasPrevious()) {
            model.addAttribute("firstPage", false);
        }
        if (addons.hasNext()) {
            model.addAttribute("lastPage", false);
        }

        model.addAttribute("page", page);

        return model;
    }
}
