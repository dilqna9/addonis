package com.telerik.addonis.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/*
table of list types containing the name of the list and the id of every entry
 */

@Getter
@Setter
@NoArgsConstructor
@Table(name = "list_types")
@Entity
public class ListType {

    @Id
    @GeneratedValue(generator = "uuid-string")
    @GenericGenerator(name = "uuid-string", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "list_id", nullable = false, unique = true, updatable = false)
    private String id;

    @Column(name = "type")
    private String type;
}
