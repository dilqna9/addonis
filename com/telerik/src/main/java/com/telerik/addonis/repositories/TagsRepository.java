package com.telerik.addonis.repositories;

import com.telerik.addonis.models.Tag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

/*repository for tags allowing functionalities for create/get tag
boolean check for existing tag*/

@Repository
public interface TagsRepository extends JpaRepository<Tag, String> {

    boolean existsByName(String tagName);

    boolean existsById(String tagId);

    Tag findByName(String tagName);

    List<Tag> findAllByName(String tagName);
}
