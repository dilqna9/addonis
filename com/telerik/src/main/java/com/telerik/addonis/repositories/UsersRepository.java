package com.telerik.addonis.repositories;

import com.telerik.addonis.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/*repository for users allowing functionalities for create/update/get/delete user
filter and sort users
boolean checks for existing users*/

@Repository
public interface UsersRepository extends JpaRepository<User, String>{

    boolean existsById(String userId);

    boolean existsByIdAndEnabledIsTrue(String userId);

    boolean existsByUsername(String username);

    boolean existsByUsernameAndEnabledIsTrue(String username);

    boolean existsByUsernameAndEnabledIsFalse(String username);

    List<User> findAllByEnabledIsTrue();

    List<User> findAllByEnabledIsFalse();

    List<User> findAllByUsernameAndEnabledIsTrue(String username);

    User findByIdAndEnabledIsTrue(String userId);

    Optional<User> findById(String userId);

    User findByUsernameAndEnabledIsTrue(String username);

    User findByUsernameAndEnabledIsFalse(String username);

}
