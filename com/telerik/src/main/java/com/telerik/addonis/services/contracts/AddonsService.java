package com.telerik.addonis.services.contracts;

import com.telerik.addonis.models.*;
import org.springframework.data.domain.Page;

import java.util.List;

public interface AddonsService {

    void createAddon(Addon addon, GitHubData gitHubData);

    void updateAddon(Addon addon);

    boolean checkAddonExistsByOriginLink(String originLink);

    Page<Addon> getAllByPage(int pageNumber);

    List<Addon> getAll();

    List<Addon> getAllPendingAddons();

    Addon getById(String addonId);

    Addon getByName(String addonName);

    Page<Addon> getAllByCreatorAndPage(String creator, int page);

    List<Addon> getAllByCreator(String creator);

    List<Addon> getAllByStatus(String status);

    Page<Addon> getAllByMultipleFilters(String addonName, String creatorUsername, String ideName, String orderBy, int pageNumber, int maxResultsPerPage);

}
