package com.telerik.addonis.controllers;

import com.telerik.addonis.exceptions.EntityNotFoundException;
import com.telerik.addonis.models.Addon;
import com.telerik.addonis.services.contracts.AddonsService;
import com.telerik.addonis.services.contracts.GitHubDataService;
import com.telerik.addonis.models.mappers.AddonMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

/*MVC Controller containing implementation of updating git hub data */

@Controller
@RequestMapping("/github")
@EnableScheduling
public class GitHubDataController {

    private final AddonMapper addonMapper;
    private final AddonsService addonsService;
    private final GitHubDataService gitHubDataService;
    private final Logger logger = Logger.getLogger(GitHubDataController.class.getName());

    @Autowired
    public GitHubDataController(AddonMapper addonMapper, AddonsService addonsService, GitHubDataService gitHubDataService) {
        this.addonMapper = addonMapper;
        this.addonsService = addonsService;
        this.gitHubDataService = gitHubDataService;
    }

    @PostMapping("/{addonId}")
    public String updateGitHubDataNow(@PathVariable String addonId, Model model) {
        try {
            Addon addon = addonsService.getById(addonId);
            gitHubDataService.updateGitHubData(addonMapper.updateGitHubData(addon));

            return "redirect:/addons/" + addonId;
        }catch (EntityNotFoundException | IOException e){
            model.addAttribute("errors", e.getMessage());
            return "not-allowed";
        }
    }


    @Scheduled(fixedDelay = 86400000)
    public void updateGitHubData(){
        try {
            List<Addon> addons = addonsService.getAll();

            for (Addon addon : addons) {
                gitHubDataService.updateGitHubData(addonMapper.updateGitHubData(addon));
            }
        }catch (IOException e){
            logger.warning(e.getMessage());
        }
    }
}
