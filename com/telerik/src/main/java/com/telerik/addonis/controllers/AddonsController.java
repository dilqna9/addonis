package com.telerik.addonis.controllers;

import com.telerik.addonis.exceptions.EntityAlreadyExistsException;
import com.telerik.addonis.exceptions.EntityNotFoundException;
import com.telerik.addonis.exceptions.WrongGitHubRepoException;
import com.telerik.addonis.models.*;
import com.telerik.addonis.models.dto.AddonDTO;
import com.telerik.addonis.models.dto.FilterSortDTO;
import com.telerik.addonis.models.dto.RatingDTO;
import com.telerik.addonis.services.contracts.*;
import com.telerik.addonis.models.mappers.AddonMapper;
import com.telerik.addonis.models.mappers.RatingsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.security.Principal;
import java.util.Base64;
import java.util.List;

import static com.telerik.addonis.Constants.NOT_ALLOWED;
import static com.telerik.addonis.Constants.NO_FILE_ATTACHED_ERROR_MSG;


/*MVC Controller containing implementation of create/update and delete addon,
list all addons, filter and sort addons, rate addon
and display addon details*/


@Controller
@RequestMapping("/addons")
public class AddonsController {

    private final AddonMapper addonMapper;
    private final AddonsService addonsService;
    private final RatingsMapper ratingsMapper;
    private final RatingsService ratingsService;
    private final UsersService usersService;
    private final IDEService ideService;
    private final AddonsUsersListsService addonsUsersListsService;

    @Autowired
    public AddonsController(AddonMapper addonMapper,
                            AddonsService addonsService,
                            RatingsService ratingsService,
                            RatingsMapper ratingsMapper,
                            UsersService usersService,
                            IDEService ideService,
                            AddonsUsersListsService addonsUsersListsService) {
        this.addonMapper = addonMapper;
        this.addonsService = addonsService;
        this.ratingsService = ratingsService;
        this.ratingsMapper = ratingsMapper;
        this.usersService = usersService;
        this.ideService = ideService;
        this.addonsUsersListsService = addonsUsersListsService;
    }

    @GetMapping
    public String allAddons(Model model, @RequestParam int page) {

        Page<Addon> addons = addonsService.getAllByPage(page);
        model.addAttribute("addons", addons);
        model = supplyPageInformation(model, page, addons);
        return "addons";
    }

    @ModelAttribute("ides")
    public List<IDE> supplyIDEs() {
        return ideService.getAll();
    }

    @ModelAttribute("filterSortDTO")
    public FilterSortDTO supplyFilterSortDTO() {
        return new FilterSortDTO();
    }

    @GetMapping("/filter/{page}")
    public String filterSortAddons(@ModelAttribute FilterSortDTO filterSortDTO,
                                   Model model,
                                   @PathVariable int page) {

        Page<Addon> addons = addonsService.getAllByMultipleFilters(
                "%" + filterSortDTO.getAddonName() + "%",
                "%" + filterSortDTO.getCreatorUsername() + "%",
                "%" + filterSortDTO.getIdeName() + "%",
                filterSortDTO.getOrderBy(),
                page, 8);
        model.addAttribute("addons", addons.getContent());

        model = supplyPageInformation(model, page, addons);

        return "addons";
    }

    @GetMapping("/new")
    public String createAddon(Model model) {
        model.addAttribute("addonDTO", new AddonDTO());
        model.addAttribute("tag", new Tag());
        return "create-addon";
    }

    @PostMapping("/new")
    public String createAddon(@ModelAttribute @Valid AddonDTO addonDTO,
                              Model model, Principal principal) {

        try {
            if (principal == null) {
                model.addAttribute("errors", NOT_ALLOWED);
                return "not-allowed";
            }

            if (addonDTO.getFile().isEmpty()) {
                model.addAttribute("errors", NO_FILE_ATTACHED_ERROR_MSG);
                return "create-addon";
            }

            GitHubData gitHubData = addonMapper.findGitHubData(addonDTO.getOriginLink());

            Addon addon = addonMapper.mapAddonDTOtoAddon(addonDTO, principal.getName(), gitHubData);

            if (!addonDTO.getImage().isEmpty()) {
                addon.setAddonImage(Base64.getEncoder().encodeToString(addonDTO.getImage().getBytes()));
            }

            addonsService.createAddon(addon, gitHubData);

            addonMapper.setDownloadLinkToAddon(addon, addonDTO);
            addonsService.updateAddon(addon);

            model.addAttribute("addon", addon);
            model.addAttribute("tag", new Tag());

            return "add-tag";
        } catch (EntityAlreadyExistsException | WrongGitHubRepoException | IOException e) {
            model.addAttribute("errors", e.getMessage());
            return "create-addon";
        }
    }

    @GetMapping("/{addonId}")
    public String addonDetails(@PathVariable String addonId, Model model, Principal principal) {
        try {
            Addon addon = addonsService.getById(addonId);
            RatingDTO ratingDTO = new RatingDTO();
            ratingDTO.setAvgRating(ratingsService.getAvgRatingOfAddon(addonId));

            String role = usersService.getUserRole(principal, addon.getCreatedBy().getUsername());
            String status = addon.getStatus().getName();
            model.addAttribute("status", status);
            model.addAttribute("addon", addon);
            model.addAttribute("ratingDTO", ratingDTO);
            model.addAttribute("role", role);
            model.addAttribute("adminList", addonsUsersListsService.getAllAddonsByListType("admin"));

            return "addon";
        }catch (EntityNotFoundException e){
            model.addAttribute("errors", e.getMessage());
            return "not-allowed";
        }
    }

    @PostMapping("/{addonId}/update")
    public String updateAddon(@ModelAttribute @Valid AddonDTO addonDTO,
                              @PathVariable String addonId,
                              Model model, Principal principal) {
        try {
            Addon addon = addonsService.getById(addonId);

            String role = usersService.getUserRole(principal, addon.getCreatedBy().getUsername());
            if (!(role.equals("ADMIN") || role.equals("CREATOR"))) {
                model.addAttribute("errors", NOT_ALLOWED);
                return "not-allowed";
            }

            addon = addonMapper.mapAddonDTOtoAddon(addonDTO, addon);

            if (!addonDTO.getImage().isEmpty()) {
                addon.setAddonImage(Base64.getEncoder().encodeToString(addonDTO.getImage().getBytes()));
            }

            if (!addonDTO.getFile().isEmpty()) {
                addon.setFile(Base64.getEncoder().encode(addonDTO.getFile().getBytes()));
                addonMapper.setDownloadLinkToAddon(addon, addonDTO);
            }

            addonsService.updateAddon(addon);
            model.addAttribute("addon", addon);

            return "redirect:/addons/" + addonId;
        } catch (EntityNotFoundException | IOException e) {
            model.addAttribute("errors", e.getMessage());
            return "create-addon";
        }
    }


    @GetMapping("/{addonId}/update")
    public String getUpdateAddon(@PathVariable String addonId, Model model, Principal principal) {
        try {
            Addon addon = addonsService.getById(addonId);

            String role = usersService.getUserRole(principal, addon.getCreatedBy().getUsername());
            if (!(role.equals("ADMIN") || role.equals("CREATOR"))) {
                model.addAttribute("errors", NOT_ALLOWED);
                return "not-allowed";
            }
            AddonDTO addonDTO = addonMapper.mapAddonToAddonDTO(addon);

            model.addAttribute("role", role);
            model.addAttribute("addonDTO", addonDTO);
            return "update-addon";
        } catch (EntityNotFoundException e) {
            model.addAttribute("errors", e.getMessage());
            return "update-addon";
        }
    }

    @PostMapping("/{addonId}/rate")
    public String rateAddon(@ModelAttribute RatingDTO ratingDTO,
                            @PathVariable String addonId,
                            Principal principal) {

        Rating rating = ratingsMapper.mapRatingDtoToRating(addonId, principal.getName(), ratingDTO);
        ratingsService.addRatingToAddon(rating);

        return "redirect:/addons/" + addonId;
    }


    @PostMapping("/{addonId}/remove")
    public String removeAddon(@PathVariable String addonId, Model model, Principal principal) {
        try {
            Addon addon = addonsService.getById(addonId);
            addon.setEnabled(false);

            String role = usersService.getUserRole(principal, addon.getCreatedBy().getUsername());
            if (!(role.equals("ADMIN") || role.equals("CREATOR"))) {
                model.addAttribute("errors", NOT_ALLOWED);
                return "not-allowed";
            }

            addonsService.updateAddon(addon);

            return "redirect:/addons/filter/1?addonName=&creatorUsername=&ideName=&orderBy=addon_name";
        } catch (EntityNotFoundException e) {
            model.addAttribute("errors", e.getMessage());
            return "not-allowed";
        }
    }

    private Model supplyPageInformation(Model model, int page, Page<Addon> addons) {
        if (addons.hasPrevious()) {
            model.addAttribute("firstPage", false);
        }
        if (addons.hasNext()) {
            model.addAttribute("lastPage", false);
        }

        model.addAttribute("page", page);

        return model;
    }
}
