package com.telerik.addonis.services;

import com.telerik.addonis.exceptions.EntityNotFoundException;
import com.telerik.addonis.models.Rating;
import com.telerik.addonis.repositories.RatingsRepository;
import com.telerik.addonis.services.contracts.RatingsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.telerik.addonis.Constants.NOT_FOUND_ERROR_MSG;

/*service for rating creating implementation for create/update/get rating
and throwing exceptions where appropriate*/

@Service
public class RatingsServiceImpl implements RatingsService {

    private final RatingsRepository ratingsRepository;

    @Autowired
    public RatingsServiceImpl(RatingsRepository ratingsRepository) {
        this.ratingsRepository = ratingsRepository;
    }

    @Override
    public void deleteById(String ratingId) {
        if (!ratingsRepository.existsById(ratingId)) {
            throw new EntityNotFoundException(String.format(NOT_FOUND_ERROR_MSG, "Rating", "ID", ratingId));
        }
        ratingsRepository.deleteById(ratingId);
    }

    @Override
    public void addRatingToAddon(Rating rating) {
        if (ratingsRepository.existsByUser_IdAndAddon_Id(
                rating.getUser().getId(), rating.getAddon().getId())) {

            Rating oldRating = ratingsRepository.
                    findByUser_IdAndAddon_Id(rating.getUser().getId(), rating.getAddon().getId());
            oldRating.setRating(rating.getRating());

            ratingsRepository.saveAndFlush(oldRating);

        }else {
            ratingsRepository.saveAndFlush(rating);
        }
    }

    @Override
    public Rating getRatingByCreator(String username) {
        if (!ratingsRepository.existsByUser_Username(username)) {
            throw new EntityNotFoundException(String.format(NOT_FOUND_ERROR_MSG, "Rating", "creator", username));
        }
        return ratingsRepository.findByUser_Username(username);
    }

    @Override
    public Double getAvgRatingOfAddon(String addonId) {
        return ratingsRepository.findAverageRatingOfAddon(addonId);
    }
}
