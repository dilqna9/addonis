package com.telerik.addonis.repositories;

import com.telerik.addonis.models.Rating;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.io.StringReader;
import java.util.List;

/*repository for rating allowing functionalities for create/update/get rating of user
boolean check for existing rating*/

@Repository
public interface RatingsRepository extends JpaRepository<Rating, String> {

    boolean existsById(String ratingId);

    boolean existsByUser_Username(String username);

    boolean existsByUser_IdAndAddon_Id(String userId, String addonId);

    void deleteById(String ratingId);

    Rating findByUser_Username(String username);

    Rating findByUser_IdAndAddon_Id(String userId, String addonId);

    @Query("select avg(r.rating) from Rating r where r.addon.id = :#{#addonId}")
    Double findAverageRatingOfAddon(String addonId);


}
