package com.telerik.addonis.repositories;

import com.telerik.addonis.models.User;
import com.telerik.addonis.models.VerificationToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/*
repository for tokens having find an create functionalities
 */

@Repository
public interface VerificationTokenRepository extends JpaRepository<VerificationToken, String> {

    VerificationToken findByToken(String token);

    VerificationToken findByUser(User user);
}
