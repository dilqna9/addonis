package com.telerik.addonis.services;

import com.telerik.addonis.exceptions.EntityAlreadyExistsException;
import com.telerik.addonis.exceptions.EntityNotFoundException;
import com.telerik.addonis.models.Addon;
import com.telerik.addonis.models.GitHubData;
import com.telerik.addonis.repositories.AddonsRepository;
import com.telerik.addonis.repositories.GitHubDataRepository;
import com.telerik.addonis.services.contracts.AddonsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.telerik.addonis.Constants.ALREADY_EXITS_ERROR_MSG;
import static com.telerik.addonis.Constants.NOT_FOUND_ERROR_MSG;

/*service for addons creating implementation for create/update/get/delete addon
filter and sort addons
and throwing exceptions where appropriate*/

@Service
public class AddonsServiceImpl implements AddonsService {

    private final AddonsRepository addonsRepository;
    private final GitHubDataRepository gitHubDataRepository;

    @Autowired
    public AddonsServiceImpl(AddonsRepository addonsRepository, GitHubDataRepository gitHubDataRepository) {
        this.addonsRepository = addonsRepository;
        this.gitHubDataRepository = gitHubDataRepository;
    }

    @Override
    public void createAddon(Addon addon, GitHubData gitHubData) {
        if (addonsRepository.existsByOriginLinkAndEnabledIsTrue(addon.getOriginLink())) {
            throw new EntityAlreadyExistsException(
                    String.format(ALREADY_EXITS_ERROR_MSG, "Addon", "name", addon.getName()));
        }

        if (addonsRepository.existsByOriginLinkAndEnabledIsFalse(addon.getOriginLink())) {
            Addon oldAddon = addonsRepository.findByOriginLink(addon.getOriginLink());
            addon.setId(oldAddon.getId());
            gitHubData.setId(oldAddon.getGitHubData().getId());
        }
        gitHubDataRepository.saveAndFlush(gitHubData);
        addonsRepository.saveAndFlush(addon);
    }

    @Override
    public void updateAddon(Addon addon) {
        if (!addonsRepository.existsById(addon.getId())) {
            throw new EntityNotFoundException(
                    String.format(NOT_FOUND_ERROR_MSG, "Addon", "ID", addon.getId()));
        }

        addonsRepository.saveAndFlush(addon);
    }

    @Override
    public boolean checkAddonExistsByOriginLink(String originLink) {
        return addonsRepository.existsByOriginLinkAndEnabledIsTrue(originLink);
    }

    @Override
    public Page<Addon> getAllByPage(int pageNumber) {
        Pageable pageable = PageRequest.of(pageNumber - 1, 8);

        return addonsRepository.findAllByEnabledIsTrueAndStatus_Name("approved", pageable);
    }

    @Override
    public List<Addon> getAll() {
        return addonsRepository.findAllByEnabledIsTrueAndStatus_Name("approved");
    }

    @Override
    public List<Addon> getAllPendingAddons() {
        return addonsRepository.findAllByEnabledIsTrueAndStatus_Name("pending");
    }

    @Override
    public Addon getById(String addonId) {
        if (!addonsRepository.existsById(addonId)) {
            throw new EntityNotFoundException(
                    String.format(NOT_FOUND_ERROR_MSG, "Addon", "ID", addonId));
        }
        return addonsRepository.findByIdAndEnabledIsTrue(addonId);
    }

    @Override
    public Addon getByName(String addonName) {
        if (!addonsRepository.existsByNameAndEnabledIsTrue(addonName)) {
            throw new EntityNotFoundException(
                    String.format(NOT_FOUND_ERROR_MSG, "Addon", "name", addonName));
        }
        return addonsRepository.findByName(addonName);
    }

    @Override
    public Page<Addon> getAllByCreatorAndPage(String creator, int pageNumber) {
        Pageable pageable = PageRequest.of(pageNumber - 1, 8);

        return addonsRepository.findAllByCreatedBy_UsernameAndEnabledIsTrue(creator, pageable);
    }

    @Override
    public List<Addon> getAllByCreator(String creator) {
        return addonsRepository.findAllByCreatedBy_UsernameAndEnabledIsTrue(creator);
    }

    @Override
    public List<Addon> getAllByStatus(String status) {
        return addonsRepository.findAllByStatus_Name(status);
    }

    @Override
    public Page<Addon> getAllByMultipleFilters(String addonName, String creatorUsername, String ideName, String orderBy, int pageNumber, int maxResultsPerPage) {
        Pageable pageable = PageRequest.of(pageNumber - 1, maxResultsPerPage);

        if (orderBy.equals("addon_name")) {
            return addonsRepository
                    .findAllByEnabledIsTrueAndStatus_NameAndNameLikeAndCreatedBy_UsernameLikeAndIde_NameLikeOrderByName("approved", addonName, creatorUsername, ideName, pageable);
        } else if (orderBy.equals("downloads_count")) {
            return addonsRepository
                    .findAllByEnabledIsTrueAndStatus_NameAndNameLikeAndCreatedBy_UsernameLikeAndIde_NameLikeOrderByDownloadsCountDesc("approved", addonName, creatorUsername, ideName, pageable);
        } else if (orderBy.equals("upload_date")) {
            return addonsRepository
                    .findAllByEnabledIsTrueAndStatus_NameAndNameLikeAndCreatedBy_UsernameLikeAndIde_NameLikeOrderByUploadDateDesc("approved", addonName, creatorUsername, ideName, pageable);
        } else {
            return addonsRepository
                    .findAllByEnabledIsTrueAndStatus_NameAndNameLikeAndCreatedBy_UsernameLikeAndIde_NameLikeOrderByGitHubData_LastCommitDateDesc("approved", addonName, creatorUsername, ideName, pageable);
        }
    }

}
