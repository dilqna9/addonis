package com.telerik.addonis.models.dto;


import com.telerik.addonis.models.Tag;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.Size;
import java.util.List;

/*
DTO for creating and updating addon
 */


@NoArgsConstructor
@Getter
@Setter
public class AddonDTO {

    @Size(min = 3, max = 50, message = "Name of addon must be between 3 and 50 characters")
    private String name;

    @Size(min = 3, max = 5000, message = "Description must be between 3 and 5000 characters")
    private String description;

    private String originLink;

    private MultipartFile image;

    private String ideName;

    private MultipartFile file;

    private List<Tag> tags;
    
}
