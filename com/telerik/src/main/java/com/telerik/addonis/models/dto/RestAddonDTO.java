package com.telerik.addonis.models.dto;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Size;


/*DTO for creating addon in rest controller*/

@NoArgsConstructor
@Getter
@Setter
public class RestAddonDTO {

    @Size(min = 3, max = 50, message = "Name of addon must be between 3 and 50 characters")
    private String name;

    @Size(min = 3, max = 5000, message = "Description must be between 3 and 5000 characters")
    private String description;

    private String originLink;

    private String ideName;

}
