package com.telerik.addonis;


/*Constants for the Addonis application*/

public final class Constants {

    public static final String GITHUB_URL_PREFIX = "https://github.com/";
    public static final String GITHUB_TOKEN = "d0bb89803405d50b30064b6f914a327eea04fbea";

    public static final String NOT_FOUND_ERROR_MSG = "%s with %s %s does not exist.";
    public static final String ALREADY_EXITS_ERROR_MSG = "%s with %s %s already exists.";
    public static final String WRONG_PASSWORD_ERROR_MSG = "Old password does not match.";
    public static final String CONFIRM_PASSWORD_ERROR_MSG = "Password confirmation does not match new password.";
    public static final String NO_FILE_ATTACHED_ERROR_MSG = "You need to attach an application file for your addon.";
    public static final String GITHUB_URL_ERROR_MSG = "GitHub URL %s is incorrect.";
    public static final String NOT_CREATOR_ERROR_MSG = "You are not the creator of this addon";

    public static final String NOT_ALLOWED = "The page you are trying to access requires additional authentication.";

    public static final String ADDON_ALREADY_ADDED_TO_LIST = "This addon is already added to list %s";

    public static final String ROLE_USER = "ROLE_USER";
}
