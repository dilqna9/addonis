package com.telerik.addonis.exceptions;


/*
Custom Exception for already existing object in the database
 */

public class EntityAlreadyExistsException extends RuntimeException {

    public EntityAlreadyExistsException(String message) {
        super(message);
    }
}
