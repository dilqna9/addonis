package com.telerik.addonis.services.contracts;

import com.telerik.addonis.models.Status;

public interface StatusService {

    Status getStatusByName(String name);
}
