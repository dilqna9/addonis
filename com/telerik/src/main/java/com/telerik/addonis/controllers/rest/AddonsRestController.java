package com.telerik.addonis.controllers.rest;

import com.telerik.addonis.exceptions.EntityAlreadyExistsException;
import com.telerik.addonis.exceptions.EntityNotFoundException;
import com.telerik.addonis.models.Addon;
import com.telerik.addonis.models.GitHubData;
import com.telerik.addonis.models.Rating;
import com.telerik.addonis.models.dto.AddonDTO;
import com.telerik.addonis.models.dto.FilterSortDTO;
import com.telerik.addonis.models.dto.RatingDTO;
import com.telerik.addonis.models.dto.RestAddonDTO;
import com.telerik.addonis.services.contracts.AddonsService;
import com.telerik.addonis.services.contracts.RatingsService;
import com.telerik.addonis.models.mappers.AddonMapper;
import com.telerik.addonis.models.mappers.RatingsMapper;
import com.telerik.addonis.models.mappers.RestAddonMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.security.Principal;
import java.util.Base64;
import java.util.List;



/* Rest Controller containing implementation of create/update and delete addon,
list all addons, filter and sort addons, rate addon
and display addon details */

@RestController
@RequestMapping("/api/addons")
public class AddonsRestController {

    private final AddonsService addonsService;
    private final AddonMapper addonMapper;
    private final RatingsMapper ratingsMapper;
    private final RatingsService ratingsService;
    private final RestAddonMapper restAddonMapper;

    @Autowired
    public AddonsRestController(AddonsService addonsService,
                                AddonMapper addonMapper,
                                RatingsMapper ratingsMapper,
                                RatingsService ratingsService,
                                RestAddonMapper restAddonMapper) {
        this.addonsService = addonsService;
        this.addonMapper = addonMapper;
        this.ratingsMapper = ratingsMapper;
        this.ratingsService = ratingsService;
        this.restAddonMapper = restAddonMapper;
    }

    @PostMapping("/create")
    public Addon createAddon(@RequestBody RestAddonDTO restAddonDTO,
                             Principal principal) {
        try {
            GitHubData gitHubData = addonMapper.findGitHubData(restAddonDTO.getOriginLink());

            Addon addon = restAddonMapper.mapAddonDTOtoAddon(restAddonDTO, principal.getName(), gitHubData);

            addonsService.createAddon(addon, gitHubData);

            return addon;
        } catch (EntityAlreadyExistsException | IOException | EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.valueOf(400), e.getMessage());
        }
    }

    @PutMapping("/{addonId}/addFile")
    public Addon addFileToAddon(@PathVariable String addonId, MultipartFile file) {
        try {
            Addon addon = addonsService.getById(addonId);

            if (file != null) {
                addon.setFile(Base64.getEncoder().encode(file.getBytes()));
                restAddonMapper.setDownloadLinkToAddon(addon, file);
            }

            addonsService.updateAddon(addon);

            return addon;
        }catch (EntityNotFoundException | IOException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("/{addonId}/addPicture")
    public Addon addPictureToAddon(@PathVariable String addonId, MultipartFile picture) {
        try {
            Addon addon = addonsService.getById(addonId);

            if (picture != null) {
                addon.setAddonImage(Base64.getEncoder().encodeToString(picture.getBytes()));
            }
            addonsService.updateAddon(addon);

            return addon;
        }catch (EntityNotFoundException | IOException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("/{addonId}")
    public Addon updateAddon(@PathVariable String addonId, @RequestBody AddonDTO addonDTO) {
        try {
            Addon addon = addonsService.getById(addonId);
            addon = addonMapper.mapAddonDTOtoAddon(addonDTO, addon);

            if (!addonDTO.getImage().isEmpty()) {
                addon.setAddonImage(Base64.getEncoder().encodeToString(addonDTO.getImage().getBytes()));
            }

            if (!addonDTO.getFile().isEmpty()) {
                addon.setFile(Base64.getEncoder().encode(addonDTO.getFile().getBytes()));
                addonMapper.setDownloadLinkToAddon(addon, addonDTO);
            }

            addonsService.updateAddon(addon);

            return addon;

        } catch (EntityNotFoundException | IOException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping
    public List<Addon> getAllAddons() {
        return addonsService.getAll();
    }

    @GetMapping("/id/{id}")
    public Addon getAddonById(@PathVariable String addonId) {
        try {
            return addonsService.getById(addonId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/name/{name}")
    public Addon getAddonByName(@PathVariable String addonName) {
        try {
            return addonsService.getByName(addonName);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/filter")
    public List<Addon> multipleFilterOfAddons(FilterSortDTO filterSortDTO){

        return addonsService.getAllByMultipleFilters(
                "%" + filterSortDTO.getAddonName() + "%",
                "%" + filterSortDTO.getCreatorUsername() + "%",
                "%" + filterSortDTO.getIdeName() + "%",
                filterSortDTO.getOrderBy(),
                1,
                Integer.MAX_VALUE).getContent();
    }


    @PostMapping("/{addonId}/rate")
    public Addon rateAddon(@RequestBody RatingDTO ratingDTO,
                            @PathVariable String addonId,
                            Principal principal) {

        try {
            Rating rating = ratingsMapper.mapRatingDtoToRating(addonId, principal.getName(), ratingDTO);
            ratingsService.addRatingToAddon(rating);

            return addonsService.getById(addonId);
        }catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{addonId}")
    public Addon removeAddon(@PathVariable String addonId) {
        try {
            Addon addon = addonsService.getById(addonId);

            addon.setEnabled(false);

            addonsService.updateAddon(addon);

            return addon;
        }catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
