package com.telerik.addonis.controllers;

import com.telerik.addonis.exceptions.EntityAlreadyExistsException;
import com.telerik.addonis.exceptions.EntityNotFoundException;
import com.telerik.addonis.models.Addon;
import com.telerik.addonis.models.Tag;
import com.telerik.addonis.models.User;
import com.telerik.addonis.models.dto.RatingDTO;
import com.telerik.addonis.services.contracts.AddonsService;
import com.telerik.addonis.services.contracts.RatingsService;
import com.telerik.addonis.services.contracts.TagsService;
import com.telerik.addonis.services.contracts.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;

import static com.telerik.addonis.Constants.NOT_CREATOR_ERROR_MSG;


/* MVC Controller containing implementation of add/remove tag from addon functionality
Also calls TagsService and creates Tag if tag doesn't exist by name */

@Controller
public class TagsController {

    private final TagsService tagsService;
    private final AddonsService addonsService;
    private final UsersService usersService;
    private final RatingsService ratingsService;

    @Autowired
    public TagsController(TagsService tagsService,
                          AddonsService addonsService,
                          UsersService usersService,
                          RatingsService ratingsService) {
        this.tagsService = tagsService;
        this.addonsService = addonsService;
        this.usersService = usersService;
        this.ratingsService = ratingsService;
    }

    @ModelAttribute
    public Tag supplyTag() {
        return new Tag();
    }

    @GetMapping("addon/{addonId}/tags")
    public String addTagToAddon(@PathVariable String addonId, Model model, Principal principal) {
        Addon addon = addonsService.getById(addonId);
        User user = usersService.getByUsernameAndEnabledIsTrue(principal.getName());

        if (!user.getId().equals(addon.getCreatedBy().getId())) {

            model.addAttribute("errors", NOT_CREATOR_ERROR_MSG);
            model.addAttribute("addon", addon);
            RatingDTO ratingDTO = new RatingDTO();
            ratingDTO.setAvgRating(ratingsService.getAvgRatingOfAddon(addon.getId()));
            model.addAttribute("ratingDTO", ratingDTO);

            String role = usersService.getUserRole(principal, addon.getCreatedBy().getUsername());
            model.addAttribute("role", role);

            return "addon";
        }

        model.addAttribute("addon", addon);
        return "add-tag";
    }

    @PostMapping("addon/{addonId}/tags/new")
    public String addTagToAddon(@ModelAttribute @Valid Tag tag,
                                @PathVariable String addonId) {

        Addon addon = addonsService.getById(addonId);

        tag.setName(tag.getName().toLowerCase());
        try {
            tagsService.createTag(tag);
        } catch (EntityAlreadyExistsException e) {
            tag = tagsService.findByName(tag.getName());
        }

        addon.getTags().add(tag);
        addonsService.updateAddon(addon);

        return "redirect:/addon/" + addonId + "/tags";
    }

    @PostMapping("addon/{addonId}/tags/remove")
    public String removeTagFromAddon(@ModelAttribute Tag tag,
                                     @PathVariable String addonId,
                                     Model model) {
        try {
            Addon addon = addonsService.getById(addonId);

            tag = tagsService.findByName(tag.getName().toLowerCase());

            addon.getTags().remove(tag);
            addonsService.updateAddon(addon);
            return "redirect:/addon/" + addonId + "/tags";
        } catch (EntityNotFoundException e) {
            Addon addon = addonsService.getById(addonId);
            model.addAttribute("addon", addon);
            model.addAttribute("errors", e.getMessage());
            return "add-tag";
        }

    }
}
