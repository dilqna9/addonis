package com.telerik.addonis.controllers.rest;

import com.telerik.addonis.exceptions.EntityAlreadyExistsException;
import com.telerik.addonis.exceptions.EntityNotFoundException;
import com.telerik.addonis.models.User;
import com.telerik.addonis.models.UserDetails;
import com.telerik.addonis.models.dto.*;
import com.telerik.addonis.services.contracts.UsersService;
import com.telerik.addonis.models.mappers.RestUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.security.Principal;
import java.util.Base64;
import java.util.List;

import static com.telerik.addonis.Constants.CONFIRM_PASSWORD_ERROR_MSG;
import static com.telerik.addonis.Constants.WRONG_PASSWORD_ERROR_MSG;

/*Rest Controller containing implementation of creating/updating/listing users */

@RestController
@RequestMapping("/api/users")
public class UsersRestController {

    private final UsersService usersService;
    private final RestUserMapper restUserMapper;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UsersRestController(UsersService usersService, RestUserMapper restUserMapper, PasswordEncoder passwordEncoder) {
        this.usersService = usersService;
        this.restUserMapper = restUserMapper;
        this.passwordEncoder = passwordEncoder;
    }

    @PostMapping("/register")
    public User register(@RequestBody RestUserDTO restUserDTO) {
        try {

            UserDetails userDetails = restUserMapper.mapUserDtoToUserDetails(restUserDTO);
            User user = restUserMapper.mapUserDtoToUser(restUserDTO, userDetails);

            usersService.createUser(user, userDetails);
            return user;

        } catch (EntityAlreadyExistsException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }


    @PutMapping("/{username}/details")
    public User updateUserDetails(@PathVariable String username,@RequestBody RestUserDetailsDTO restUserDetailsDTO){
        try {
            User user = usersService.getByUsernameAndEnabledIsTrue(username);
            restUserMapper.mapUserDetailsDtoToUserDetails(restUserDetailsDTO, username);

            usersService.updateUser(user);
            return user;
        }catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("/{userId}/addPicture")
    public User addPictureToUser(@PathVariable String userId, MultipartFile image){

        try {
            User user = usersService.getUserByIdAndEnabledIsTrue(userId);

            if (image != null) {
                user.getUserDetails().setImage(Base64.getEncoder().encodeToString(image.getBytes()));
            }

            usersService.updateUser(user);

            return user;
        }catch (EntityNotFoundException | IOException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public List<User> getAllUsersAndEnabledIsTrue() {
        return usersService.getAllByEnabledIsTrue();
    }

    @GetMapping("/name/{username}")
    public User getUserByNameAndEnabledIsTrue(@PathVariable String username) {
        try {
            return usersService.getByUsernameAndEnabledIsTrue(username);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }


    @PutMapping("/update/password")
    public User changePassword(@RequestBody UserPasswordDTO userPasswordDto, Principal principal) {
        try {
            User user = usersService.getByUsernameAndEnabledIsTrue(principal.getName());

            if (!passwordEncoder.matches(userPasswordDto.getOldPassword(), user.getPassword())) {
                throw new ResponseStatusException(HttpStatus.valueOf("Wrong Password"), WRONG_PASSWORD_ERROR_MSG);
            }

            if (!userPasswordDto.getPassword().equals(userPasswordDto.getPasswordConfirmation())) {
                throw new ResponseStatusException(HttpStatus.valueOf("Not Matching Passwords"), CONFIRM_PASSWORD_ERROR_MSG);
            }
            user.setPassword(passwordEncoder.encode(userPasswordDto.getPassword()));

            usersService.updateUser(user);
            return user;

        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
