package com.telerik.addonis.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;


/*user table class containing the credentials of users and foreign key for the user details class*/

@Entity
@Table(name = "users")
@NoArgsConstructor
@Getter
@Setter
public class User {

    @Id
    @GeneratedValue(generator = "uuid-string")
    @GenericGenerator(name = "uuid-string", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "user_id", nullable = false, unique = true, updatable = false)
    private String id;

    @Column(name = "username", nullable = false)
    @Size(min = 3, max = 50, message = "Username must be between 3 and 15 characters")
    private String username;

    @Column(name = "password", nullable = false)
    @Size(min = 6, max = 68, message = "Password must be between 6 and 20 characters")
    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{6,}$")
    private String password;

    @Column(name = "enabled", nullable = false)
    private boolean enabled;

    @OneToOne
    @JoinColumn(name = "user_details_id", nullable = false)
    private UserDetails userDetails;

}
