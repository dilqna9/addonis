package com.telerik.addonis.services;

import com.telerik.addonis.models.IDE;
import com.telerik.addonis.repositories.IDERepository;
import com.telerik.addonis.services.contracts.IDEService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/*service for IDEs creating implementation for get IDE*/


@Service
public class IDEServiceImpl implements IDEService {

    private final IDERepository ideRepository;

    @Autowired
    public IDEServiceImpl(IDERepository ideRepository) {
        this.ideRepository = ideRepository;
    }

    @Override
    public List<IDE> getAll() {
        return ideRepository.findAll();
    }

    @Override
    public IDE getByName(String ideName) {
        return ideRepository.findByName(ideName);
    }
}
