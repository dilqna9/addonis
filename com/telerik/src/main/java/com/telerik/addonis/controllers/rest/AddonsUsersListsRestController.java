package com.telerik.addonis.controllers.rest;

import com.telerik.addonis.exceptions.EntityNotFoundException;
import com.telerik.addonis.models.Addon;
import com.telerik.addonis.models.AddonUserList;
import com.telerik.addonis.models.ListType;
import com.telerik.addonis.models.User;
import com.telerik.addonis.models.dto.FilterSortDTO;
import com.telerik.addonis.services.contracts.AddonsService;
import com.telerik.addonis.services.contracts.AddonsUsersListsService;
import com.telerik.addonis.services.contracts.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.security.Principal;
import java.util.List;

/* Rest Controller containing implementation of adding/removing addon from a user list */

@RestController
@RequestMapping("/api/lists")
public class AddonsUsersListsRestController {

    private final AddonsUsersListsService addonsUsersListsService;
    private final UsersService usersService;
    private final AddonsService addonsService;

    @Autowired
    public AddonsUsersListsRestController(AddonsUsersListsService addonsUsersListsService, UsersService usersService, AddonsService addonsService) {
        this.addonsUsersListsService = addonsUsersListsService;
        this.usersService = usersService;
        this.addonsService = addonsService;
    }

    @GetMapping("/{listType}")
    public List<Addon> getAllByUserAndList(@PathVariable String listType){

        return addonsUsersListsService.getAllAddonsByListType(listType);
    }

    @PostMapping("/{addonId}/{listType}/add")
    public List<Addon> addAddonToAddonUserList(@PathVariable String addonId,
                                               @PathVariable String listType,
                                               Principal principal) {

        try {
            if (!usersService.getUserRole(principal, "").equals("ADMIN")) {
                throw new ResponseStatusException(HttpStatus.METHOD_NOT_ALLOWED);
            }

            Addon addon = addonsService.getById(addonId);
            User user = usersService.getByUsernameAndEnabledIsTrue(principal.getName());
            ListType type = addonsUsersListsService.getByType(listType);

            AddonUserList addonUserList = new AddonUserList();

            addonUserList.setAddon(addon);
            addonUserList.setUser(user);
            addonUserList.setListType(type);

            addonsUsersListsService.createAddonUserList(addonUserList);

            return addonsUsersListsService.getAllAddonsByListType(listType);
        }catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/{addonId}/{listType}/remove")
    public List<Addon> removeAddonFromAddonUserList(@PathVariable String addonId,
                                               @PathVariable String listType,
                                               Principal principal) {

        try {
            if (!usersService.getUserRole(principal, "").equals("ADMIN")) {
                throw new ResponseStatusException(HttpStatus.METHOD_NOT_ALLOWED);
            }

            ListType type = addonsUsersListsService.getByType(listType);

            AddonUserList addonUserList = addonsUsersListsService.getByAddon_IdAndListType_Id(addonId, type.getId());

            addonsUsersListsService.deleteAddonUserList(addonUserList);

            return addonsUsersListsService.getAllAddonsByListType(listType);
        }catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
