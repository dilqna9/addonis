package com.telerik.addonis.models.mappers;

import com.telerik.addonis.models.Addon;
import com.telerik.addonis.models.Rating;
import com.telerik.addonis.models.User;
import com.telerik.addonis.models.dto.RatingDTO;
import com.telerik.addonis.services.contracts.AddonsService;
import com.telerik.addonis.services.contracts.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/*mapper for create and update functionality of rating*/

@Component
public class RatingsMapper {

    private final AddonsService addonsService;
    private final UsersService usersService;

    @Autowired
    public RatingsMapper(AddonsService addonsService, UsersService usersService) {
        this.addonsService = addonsService;
        this.usersService = usersService;
    }

    public Rating mapRatingDtoToRating(String addonId, String username, RatingDTO ratingDTO){
        User user = usersService.getByUsernameAndEnabledIsTrue(username);
        Addon addon = addonsService.getById(addonId);

        Rating rating = new Rating();
        rating.setRating(ratingDTO.getVote());
        rating.setUser(user);
        rating.setAddon(addon);

        return rating;
    }
}
