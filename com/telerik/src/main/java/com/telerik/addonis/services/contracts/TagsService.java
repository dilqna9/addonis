package com.telerik.addonis.services.contracts;

import com.telerik.addonis.models.Tag;

import java.util.List;

public interface TagsService {

    void createTag(Tag tag);

    Tag findByName(String tagName);

}
