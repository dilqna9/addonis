package com.telerik.addonis.models.mappers;

import com.telerik.addonis.models.User;
import com.telerik.addonis.models.UserDetails;
import com.telerik.addonis.models.dto.RestUserDTO;
import com.telerik.addonis.models.dto.RestUserDetailsDTO;
import com.telerik.addonis.services.contracts.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

/*mapper for create, update and change password functionality of users and user details through rest service*/

@Component
public class RestUserMapper {

    private final PasswordEncoder passwordEncoder;
    private final UsersService usersService;

    @Autowired
    public RestUserMapper(PasswordEncoder passwordEncoder, UsersService usersService) {
        this.passwordEncoder = passwordEncoder;
        this.usersService = usersService;
    }


    public UserDetails mapUserDtoToUserDetails(RestUserDTO restUserDTO) {

        UserDetails userDetails = new UserDetails();

        userDetails.setEmail(restUserDTO.getEmail());
        userDetails.setFirstName(restUserDTO.getFirstName());
        userDetails.setLastName(restUserDTO.getLastName());

        return userDetails;
    }


    public User mapUserDtoToUser(RestUserDTO restUserDTO, UserDetails userDetails){
        User user = new User();

        user.setUsername(restUserDTO.getUsername());
        user.setPassword(passwordEncoder.encode(restUserDTO.getPassword()));
        user.setEnabled(true);
        user.setUserDetails(userDetails);

        return user;
    }

    public UserDetails mapUserDetailsDtoToUserDetails(RestUserDetailsDTO restUserDetailsDTO, String username) {

        User user = usersService.getByUsernameAndEnabledIsTrue(username);
        UserDetails userDetails = user.getUserDetails();

        userDetails.setFirstName(restUserDetailsDTO.getFirstName());
        userDetails.setLastName(restUserDetailsDTO.getLastName());
        userDetails.setEmail(restUserDetailsDTO.getEmail());


        return userDetails;
    }
}
