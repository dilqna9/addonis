package com.telerik.addonis.repositories;

import com.telerik.addonis.models.Status;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/*repository for statuses allowing functionalities for get status*/

@Repository
public interface StatusRepository extends JpaRepository<Status, String> {

    Status findByName(String name);

    boolean existsByName(String name);
}
