package com.telerik.addonis.controllers;

import com.telerik.addonis.models.Addon;
import com.telerik.addonis.models.AddonUserList;
import com.telerik.addonis.models.ListType;
import com.telerik.addonis.models.User;
import com.telerik.addonis.models.dto.FilterSortDTO;
import com.telerik.addonis.services.contracts.AddonsService;
import com.telerik.addonis.services.contracts.AddonsUsersListsService;
import com.telerik.addonis.services.contracts.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

import static com.telerik.addonis.Constants.NOT_ALLOWED;


/* MVC Controller containing implementation of adding/removing addon from a user list */

@Controller
@RequestMapping("/lists")
public class AddonsUsersListsController {

    private final AddonsUsersListsService addonsUsersListsService;
    private final AddonsService addonsService;
    private final UsersService usersService;

    @Autowired
    public AddonsUsersListsController(AddonsUsersListsService addonsUsersListsService,
                                      AddonsService addonsService,
                                      UsersService usersService) {
        this.addonsUsersListsService = addonsUsersListsService;
        this.addonsService = addonsService;
        this.usersService = usersService;
    }

    @PostMapping("/{addonId}/{listType}/add")
    public String addAddonToAddonUserList(@PathVariable String addonId,
                                          @PathVariable String listType,
                                          Model model,
                                          Principal principal) {

        if (!usersService.getUserRole(principal, "").equals("ADMIN")) {
            model.addAttribute("errors", NOT_ALLOWED);
            return "not-allowed";
        }

        Addon addon = addonsService.getById(addonId);
        User user = usersService.getByUsernameAndEnabledIsTrue(principal.getName());
        ListType type = addonsUsersListsService.getByType(listType);

        AddonUserList addonUserList = new AddonUserList();

        addonUserList.setAddon(addon);
        addonUserList.setUser(user);
        addonUserList.setListType(type);

        addonsUsersListsService.createAddonUserList(addonUserList);

        return "redirect:/admin-addons?page=1";
    }


    @PostMapping("/{addonId}/{listType}/remove")
    public String removeAddonFromAddonUserList(@PathVariable String addonId,
                                               @PathVariable String listType,
                                               Model model,
                                               Principal principal) {

        if (!usersService.getUserRole(principal, "").equals("ADMIN")) {
            model.addAttribute("errors", NOT_ALLOWED);
            return "not-allowed";
        }

        ListType type = addonsUsersListsService.getByType(listType);

        AddonUserList addonUserList = addonsUsersListsService.getByAddon_IdAndListType_Id(addonId, type.getId());

        addonsUsersListsService.deleteAddonUserList(addonUserList);

        return "redirect:/admin-addons?page=1";
    }
}
