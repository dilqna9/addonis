package com.telerik.addonis.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;


/*addon table class containing the fields of every addon and SQL restrictions*/

@Entity
@Table(name = "addons")
@NoArgsConstructor
@Getter
@Setter
public class Addon {

    @Id
    @GeneratedValue(generator = "uuid-string")
    @GenericGenerator(name = "uuid-string", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "addon_id", nullable = false, unique = true, updatable = false)
    private String id;

    @Column(name = "name", nullable = false)
    @Size(min = 3, max = 50, message = "Name of addon must be between 3 and 50 characters")
    private String name;

    @Column(name = "description", nullable = false)
    @Size(min = 3, max = 5000, message = "Description must be between 3 and 5000 characters")
    private String description;

    @Column(name = "download_link")
    private String downloadLink;

    @Column(name = "origin_link", nullable = false)
    private String originLink;

    @ManyToOne
    @JoinColumn(name = "creator_id", nullable = false)
    private User createdBy;

    @ManyToOne
    @JoinColumn(name = "status_id", nullable = false)
    private Status status;

    @Column(name = "downloads_count")
    private int downloadsCount;

    @Lob
    @Column(name = "addon_image")
    @JsonIgnore
    private String addonImage;

    @Lob
    @Column(name = "binary_file")
    @JsonIgnore
    private  byte[] file;

    @Column(name = "upload_date", nullable = false)
    private Date uploadDate;

    @Column(name = "enabled", nullable = false)
    private boolean enabled;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "addons_tags",
            joinColumns = @JoinColumn(name = "addon_id", nullable = false),
            inverseJoinColumns = @JoinColumn(name = "tag_id", nullable = false)
    )
    private Set<Tag> tags = new HashSet<>();

    @OneToOne
    @JoinColumn(name = "git_hub_data_id", nullable = false)
    private GitHubData gitHubData;

    @ManyToOne
    @JoinColumn(name = "ide_id", nullable = false)
    private IDE ide;
}
