package com.telerik.addonis.services.contracts;

import com.telerik.addonis.models.GitHubData;
import org.kohsuke.github.GitHub;

import java.io.IOException;
import java.util.Date;
import java.util.List;

public interface GitHubDataService {

    boolean validateRepoUrl(String repo);

    void createGitHubData(GitHubData gitHubData);

    void updateGitHubData(GitHubData gitHubData);

    GitHub getGitHubConnection() throws IOException;

    String getIssuesCount(String url) throws IOException;

    String getPullsCount(String url) throws IOException;

    Date getCommitDate(String url) throws IOException;

    String getCommitTitle(String url) throws IOException;

}
