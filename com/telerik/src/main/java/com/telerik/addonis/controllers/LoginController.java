package com.telerik.addonis.controllers;


import com.telerik.addonis.models.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/*MVC Controller responsible for log out of users*/

@Controller
public class LoginController {

    @GetMapping("/logout")
    public String logout(){
        return "redirect:/";
    }

    @GetMapping("/login")
    public String login(Model model){
        model.addAttribute("user", new User());

        return "login";
    }

}
