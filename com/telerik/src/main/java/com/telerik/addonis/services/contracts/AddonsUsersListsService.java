package com.telerik.addonis.services.contracts;

import com.telerik.addonis.models.Addon;
import com.telerik.addonis.models.AddonUserList;
import com.telerik.addonis.models.ListType;
import org.springframework.data.domain.Page;

import java.util.List;

public interface AddonsUsersListsService {

    void createAddonUserList(AddonUserList addonUserList);

    void deleteAddonUserList(AddonUserList addonUserList);

    AddonUserList getByAddon_IdAndListType_Id(String addonId, String listTypeId);

    Page<Addon> getAllAddonsByListType(String listType, int pageNumber, int maxResultsPerPage);

    List<Addon> getAllAddonsByListType(String listType);

    ListType getByType(String type);

//    ListType getById(String listTypeId);
}
