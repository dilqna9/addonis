package com.telerik.addonis.controllers.rest;

import com.telerik.addonis.exceptions.EntityAlreadyExistsException;
import com.telerik.addonis.exceptions.EntityNotFoundException;
import com.telerik.addonis.models.Addon;
import com.telerik.addonis.models.Tag;
import com.telerik.addonis.models.User;
import com.telerik.addonis.services.contracts.AddonsService;
import com.telerik.addonis.services.contracts.TagsService;
import com.telerik.addonis.services.contracts.UsersService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.security.Principal;

import static com.telerik.addonis.Constants.NOT_CREATOR_ERROR_MSG;

/*Rest Controller containing implementation of listing/adding/removing tag from addon */

@RestController
@RequestMapping("/api/tags")
public class TagsRestController {

    private final AddonsService addonsService;
    private final UsersService usersService;
    private final TagsService tagsService;

    public TagsRestController(AddonsService addonsService, UsersService usersService, TagsService tagsService) {
        this.addonsService = addonsService;
        this.usersService = usersService;
        this.tagsService = tagsService;
    }

    @GetMapping("addon/{addonId}/tags")
    public Addon addTagToAddon(@PathVariable String addonId, Principal principal) {

        try {
            Addon addon = addonsService.getById(addonId);
            User user = usersService.getByUsernameAndEnabledIsTrue(principal.getName());
            if (!user.getId().equals(addon.getCreatedBy().getId())) {
                throw new ResponseStatusException(HttpStatus.valueOf(454), NOT_CREATOR_ERROR_MSG);
            }

            return addon;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }


    @PostMapping("addon/{addonId}/tags/new")
    public Addon addTagToAddon(String tagName,
                               @PathVariable String addonId) {
        try {
            Addon addon = addonsService.getById(addonId);
            Tag tag = new Tag();
            tag.setName(tagName.toLowerCase());
            try {
                tagsService.createTag(tag);
            } catch (EntityAlreadyExistsException e) {
                tag = tagsService.findByName(tag.getName());
            }

            addon.getTags().add(tag);
            addonsService.updateAddon(addon);

            return addon;
        } catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }


    @DeleteMapping("addon/{addonId}/tags/remove")
    public Addon removeTagFromAddon(String tagName,
                                    @PathVariable String addonId) {
        try {
            Addon addon = addonsService.getById(addonId);

            Tag tag = tagsService.findByName(tagName.toLowerCase());

            addon.getTags().remove(tag);
            addonsService.updateAddon(addon);
            return addon;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
