package com.telerik.addonis.services;

import com.telerik.addonis.exceptions.EntityNotFoundException;
import com.telerik.addonis.models.Status;
import com.telerik.addonis.repositories.StatusRepository;
import com.telerik.addonis.services.contracts.StatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.telerik.addonis.Constants.NOT_FOUND_ERROR_MSG;

/*service for status creating implementation for get status*/

@Service
public class StatusServiceImpl implements StatusService {

    private final StatusRepository statusRepository;

    @Autowired
    public StatusServiceImpl(StatusRepository statusRepository) {
        this.statusRepository = statusRepository;
    }

    @Override
    public Status getStatusByName(String name) {
        if (!statusRepository.existsByName(name)) {
            throw new EntityNotFoundException(
                    String.format(NOT_FOUND_ERROR_MSG, "Status", "name", name));
        }

        return statusRepository.findByName(name);
    }
}
