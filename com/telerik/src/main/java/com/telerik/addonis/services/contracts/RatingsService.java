package com.telerik.addonis.services.contracts;

import com.telerik.addonis.models.Rating;

import java.util.List;

public interface RatingsService {

    void deleteById(String ratingId);

    void addRatingToAddon(Rating rating);

    Rating getRatingByCreator(String username);

    Double getAvgRatingOfAddon(String addonId);
}
