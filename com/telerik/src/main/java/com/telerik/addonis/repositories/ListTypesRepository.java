package com.telerik.addonis.repositories;

import com.telerik.addonis.models.ListType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/*
repository for all list types having find and boolean check
 */

@Repository
public interface ListTypesRepository extends JpaRepository<ListType,String> {

    ListType findByType(String type);

    boolean existsByType(String type);

    boolean existsById(String ListTypeId);
}
