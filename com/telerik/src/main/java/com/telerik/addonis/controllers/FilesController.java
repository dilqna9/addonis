package com.telerik.addonis.controllers;

import com.telerik.addonis.exceptions.EntityNotFoundException;
import com.telerik.addonis.models.Addon;
import com.telerik.addonis.models.User;
import com.telerik.addonis.models.UserDetails;
import com.telerik.addonis.services.contracts.AddonsService;
import com.telerik.addonis.services.contracts.UsersService;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;
import java.util.logging.Logger;


/* MVC controller for implementation of displaying images of users and addons
and downloading addon executable files*/

@Controller
public class FilesController {

    private final AddonsService addonsService;
    private final UsersService usersService;
    private final Logger logger = Logger.getLogger(FilesController.class.getName());

    @Autowired
    public FilesController(AddonsService addonsService, UsersService usersService) {
        this.addonsService = addonsService;
        this.usersService = usersService;
    }

    @GetMapping("addon/{addonId}/image")
    public void renderAddonImage(@PathVariable String addonId, HttpServletResponse response) {
        try {
            Addon addon = addonsService.getById(addonId);

            if (addon.getAddonImage() != null) {
                response.setContentType("image/jpeg");
                InputStream is = new ByteArrayInputStream(Base64.getDecoder().decode(addon.getAddonImage()));
                IOUtils.copy(is, response.getOutputStream());
            }
        } catch (EntityNotFoundException | IOException e) {
            logger.warning(e.getMessage());
        }
    }


    @GetMapping("user/{userDetailsId}/image")
    public void renderUserImage(@PathVariable String userDetailsId, HttpServletResponse response) {
        try {
            UserDetails userDetails = usersService.getUserDetailsById(userDetailsId);
            if (userDetails.getImage() != null) {
                response.setContentType("image/jpeg");
                InputStream is = new ByteArrayInputStream(Base64.getDecoder().decode(userDetails.getImage()));
                IOUtils.copy(is, response.getOutputStream());
            }
        } catch (EntityNotFoundException | IOException e) {
            logger.warning(e.getMessage());
        }
    }


    @GetMapping("/download/{addonId}/{fileName:.+}")
    public ResponseEntity downloadFromDB(@PathVariable String addonId,
                                         @PathVariable String fileName) {
        Addon addon = addonsService.getById(addonId);
        addon.setDownloadsCount(addon.getDownloadsCount() + 1);
        addonsService.updateAddon(addon);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fileName + "\"")
                .body(Base64.getDecoder().decode(addon.getFile()));
    }
}
