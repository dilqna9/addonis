package com.telerik.addonis.controllers.rest;


import com.telerik.addonis.exceptions.EntityNotFoundException;
import com.telerik.addonis.models.Addon;
import com.telerik.addonis.services.contracts.AddonsService;
import com.telerik.addonis.services.contracts.StatusService;
import com.telerik.addonis.services.contracts.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.security.Principal;

/*Rest Controller containing implementation of approving addons, enabling/disabling users
and viewing list of all users */

@RestController
@RequestMapping("/api/admin")
public class AdminRestController {

    private final UsersService usersService;
    private final AddonsService addonsService;
    private final StatusService statusService;

    @Autowired
    public AdminRestController(UsersService usersService, AddonsService addonsService, StatusService statusService) {
        this.usersService = usersService;
        this.addonsService = addonsService;
        this.statusService = statusService;
    }

    @PutMapping("/{addonId}/approve")
    public Addon approveAddon(@PathVariable String addonId, Principal principal) {

        try {
            if (!usersService.getUserRole(principal, "").equals("ADMIN")) {
                throw new ResponseStatusException(HttpStatus.METHOD_NOT_ALLOWED);
            }

            Addon addon = addonsService.getById(addonId);

            addon.setStatus(statusService.getStatusByName("approved"));

            addonsService.updateAddon(addon);

            return addon;
        }catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
