package com.telerik.addonis.services;

import com.telerik.addonis.exceptions.EntityAlreadyExistsException;
import com.telerik.addonis.exceptions.EntityNotFoundException;
import com.telerik.addonis.models.Addon;
import com.telerik.addonis.models.AddonUserList;
import com.telerik.addonis.models.ListType;
import com.telerik.addonis.repositories.AddonsUsersListsRepository;
import com.telerik.addonis.repositories.ListTypesRepository;
import com.telerik.addonis.services.contracts.AddonsUsersListsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.telerik.addonis.Constants.*;

/*service for lists of addons creating implementation for create/update/get/delete list
and throwing exceptions where appropriate*/

@Service
public class AddonsUsersListsServiceImpl implements AddonsUsersListsService {

    private final AddonsUsersListsRepository addonsUsersListsRepository;
    private final ListTypesRepository listTypesRepository;

    @Autowired
    public AddonsUsersListsServiceImpl(AddonsUsersListsRepository addonsUsersListsRepository,
                                       ListTypesRepository listTypesRepository) {
        this.addonsUsersListsRepository = addonsUsersListsRepository;
        this.listTypesRepository = listTypesRepository;
    }

    @Override
    public void createAddonUserList(AddonUserList addonUserList) {
        if (addonsUsersListsRepository.existsByAddon_IdAndListType_Id(
                addonUserList.getAddon().getId(),
                addonUserList.getListType().getId())) {

            throw new EntityAlreadyExistsException(
                    String.format(ADDON_ALREADY_ADDED_TO_LIST, addonUserList.getListType().getType()));

        }
        addonsUsersListsRepository.saveAndFlush(addonUserList);
    }

    @Override
    public void deleteAddonUserList(AddonUserList addonUserList) {
        if (!addonsUsersListsRepository.existsByAddon_IdAndListType_Id(
                addonUserList.getAddon().getId(),
                addonUserList.getListType().getId())) {

            throw new EntityNotFoundException(
                    String.format(NOT_FOUND_ERROR_MSG, "List of addons", "ID", addonUserList.getId()));

        }

        addonsUsersListsRepository.delete(addonUserList);
    }

    @Override
    public AddonUserList getByAddon_IdAndListType_Id(String addonId, String listTypeId) {
        if (!addonsUsersListsRepository.existsByAddon_IdAndListType_Id(addonId, listTypeId)) {
            throw new EntityNotFoundException(String.format(NOT_FOUND_ERROR_MSG, "Addon", "such user and list type", ""));
        }
        return addonsUsersListsRepository.findByAddon_IdAndListType_Id(addonId, listTypeId);
    }

    @Override
    public Page<Addon> getAllAddonsByListType(String listType, int pageNumber, int maxResultsPerPage) {
        Pageable pageable = PageRequest.of(pageNumber - 1, maxResultsPerPage);

        return addonsUsersListsRepository.findAllAddonsOfUserFromList(listType, pageable);
    }

    @Override
    public List<Addon> getAllAddonsByListType(String listType) {

        return addonsUsersListsRepository.findAllAddonsOfUserFromList(listType);
    }

    @Override
    public ListType getByType(String type) {
        if (!listTypesRepository.existsByType(type)) {
            throw new EntityNotFoundException(
                    String.format(NOT_FOUND_ERROR_MSG, "List", "type", type));
        }
        return listTypesRepository.findByType(type);
    }
}
