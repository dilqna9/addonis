package com.telerik.addonis.repositories;

import com.telerik.addonis.models.IDE;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/*repository for IDEs allowing functionalities for get IDE*/

@Repository
public interface IDERepository extends JpaRepository<IDE, String> {

    List<IDE> findAll();

    IDE findByName(String ideName);
}
