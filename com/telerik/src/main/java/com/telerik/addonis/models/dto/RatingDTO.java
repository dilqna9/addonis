package com.telerik.addonis.models.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/*DTO for creating and showing rating*/

@Getter
@Setter
@NoArgsConstructor
public class RatingDTO {
    Integer vote;
    Double avgRating;
}
