package com.telerik.addonis.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;

/*authority table class containing the roles of users and the user names*/

@Entity
@Table(name = "authorities")
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class Authority {

    @Id
    @GeneratedValue(generator = "uuid-string")
    @GenericGenerator(name = "uuid-string", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "authority_id", nullable = false, unique = true, updatable = false)
    private String id;

    @Column(name = "username")
    private String username;

    @Column(name = "authority")
    private String authority;

}
