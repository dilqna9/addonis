package com.telerik.addonis.models.mappers;

import com.telerik.addonis.models.User;
import com.telerik.addonis.models.UserDetails;
import com.telerik.addonis.models.dto.UserDTO;
import com.telerik.addonis.models.dto.UserDetailsDTO;
import com.telerik.addonis.services.contracts.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

/*mapper for create, update and change password functionality of users and user details*/

@Component
public class UserMapper {

    private final PasswordEncoder passwordEncoder;
    private final UsersService usersService;

    @Autowired
    public UserMapper(PasswordEncoder passwordEncoder, UsersService usersService) {
        this.passwordEncoder = passwordEncoder;
        this.usersService = usersService;
    }

    public User mapUserDtoToUser(UserDTO userDTO, UserDetails userDetails){
        User user = new User();

        user.setUsername(userDTO.getUsername());
        user.setPassword(passwordEncoder.encode(userDTO.getPassword()));
        user.setUserDetails(userDetails);

        return user;
    }

    public UserDetails mapUserDtoToUserDetails(UserDTO userDTO) {

        UserDetails userDetails = new UserDetails();

        userDetails.setEmail(userDTO.getEmail());
        userDetails.setFirstName(userDTO.getFirstName());
        userDetails.setLastName(userDTO.getLastName());

        return userDetails;
    }

    public UserDetails mapUserDetailsDtoToUserDetails(UserDetailsDTO userDetailsDTO, String username) {

        User user = usersService.getByUsernameAndEnabledIsTrue(username);
        UserDetails userDetails = user.getUserDetails();

        userDetails.setFirstName(userDetailsDTO.getFirstName());
        userDetails.setLastName(userDetailsDTO.getLastName());
        userDetails.setEmail(userDetailsDTO.getEmail());

        return userDetails;
    }

    public UserDetailsDTO mapUserDetailsToUserDetailsDTO(User user){
        UserDetailsDTO userDetailsDTO = new UserDetailsDTO();

        userDetailsDTO.setFirstName(user.getUserDetails().getFirstName());
        userDetailsDTO.setLastName(user.getUserDetails().getLastName());
        userDetailsDTO.setEmail(user.getUserDetails().getEmail());
        userDetailsDTO.setEnabled(user.isEnabled());

        return userDetailsDTO;
    }

}
