package com.telerik.addonis.models;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/*
table class of lists of addons created by users containing the id of the user and addon
 */

@Getter
@Setter
@NoArgsConstructor
@Table(name = "addons_users_lists")
@Entity
public class AddonUserList {

    @Id
    @GeneratedValue(generator = "uuid-string")
    @GenericGenerator(name = "uuid-string", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "addon_user_list_id", nullable = false, unique = true, updatable = false)
    private String id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "addon_id")
    private Addon addon;

    @ManyToOne
    @JoinColumn(name = "list_id")
    private ListType listType;
}
