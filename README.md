# Addonis


## Description
**Addonis** is a web application that gives users the opportunity to manage add-ons. The Aplication is with three types of users: 
- anonymous;
- regular;
- administrator.

Anonymous users are able to filter addons by name or IDE and sort by the name, number of downloads, upload date, last commit date. They can also download a plugin, directly from the landing page.

Registered users have private area in the web application accessible after successful login, where they could see all extensions that are owned by the currently logged user. Additionally, the registered users are able to delete, update, create their own addons and rate all addons. Once addon is created the it is "pending" state until the administrator approves it. The extension is visible in the landing page only if it is approved.

System administrators can administer all major information objects in the system. On top of the regular user capabilities, the administrators have the following capabilities: 
- to approve new addons;
- delete or edit all addons;
- disable users accounts.


## Technologies
- Spring MVC
- Spring Security
- Hibernate
- Spring Data JPA
- Thymeleaf
- HTML
- CSS
- JUnit

## Database
![picture](images/addonis_db.png)

## WebApp Screenshots

- Landing Page (URL **"/"**)- landing page with three categories - newest addons, most popular, featured
![picture](images/landing-page.png)

- All Addons Page (URL **"addons/filter/1?addonName=&creatorUsername=&ideName=&orderBy=addon_name"**)- page with all addons with status "approved" and their filter and sort options
![picture](images/all-addons.png)

- Addon Details Page (URL **"/addons/{addonId}"**)- page with data for each addon like name, description, tags, binary content, origin link, count of downloads of binary file, rating
![picture](images/details-1.png)
![picture](images/details-2.png)

- Create new Addon (URL **"/addons/new"**)- form for create new addons with their details 
![picture](images/create-addon.png)

- Update Addon (URL **"addons/{addonId}/update"**)- form for update details (without origin link) of addon 
![picture](images/update-addon.png)

- Register Form (URL **"/users/register"**)
![picture](images/register.png)

- Login Form (URL **"/login"**)
![picture](images/login.png)

- Update User details (URL **"/users/details"**)- form for update details (without username) of user 
![picture](images/update-profile.png)

- Change password (URL **"/users/password"**)- users can change their own password in this form
![picture](images/change-password.png)

- User profile (URL **"/users/profile?page=1"**)- page with detail information for users and their own addons
![picture](images/user-profile-1.png)
![picture](images/user-profile-2.png)
![picture](images/user-profile-3.png)



Trello board https://trello.com/b/tZrfB0MC/addonis


Presentation https://prezi.com/view/lnrYNjPhbvqqz2QLuGTU/


The project was developed by **Dilyana Dimova** and **Petar Aleksiev**.
