insert into addonis_db.status(status_id, status) values ('d0fced11-fe76-4680-800b-dccd8cfb49bc', 'pending');
insert into addonis_db.status(status_id, status) values ('8a4549cb-6d84-40a1-b13b-d5ae5335c213', 'approved');


insert into addonis_db.ides(ide_id, ide_name) values ('231d7e5c-a54e-4f3e-a3f2-cbf77e0911c0', 'Anjuta');
insert into addonis_db.ides(ide_id, ide_name) values ('cb1d323e-fa71-408d-b477-d7b04c5a43db', 'C++Builder');
insert into addonis_db.ides(ide_id, ide_name) values ('d7a9331c-d800-4be5-a4d7-4b2f7b557d88', 'Code::Blocks');
insert into addonis_db.ides(ide_id, ide_name) values ('a44ee4cc-6e4c-4d19-a44d-336c9f74ea03', 'Eclipse CDT');
insert into addonis_db.ides(ide_id, ide_name) values ('34fc2f18-8d37-41a6-a5c4-216eae95b3f0', 'Microsoft Visual Studio');
insert into addonis_db.ides(ide_id, ide_name) values ('d7bbd8db-23ab-49f7-9269-ceddfa19bcdc', 'Microsoft Visual Studio Code');
insert into addonis_db.ides(ide_id, ide_name) values ('a8fbf203-da7a-4d78-962d-5cb914514b30', 'MonoDevelop');
insert into addonis_db.ides(ide_id, ide_name) values ('770f7383-6c50-4868-9826-b0250c7e59c9', 'BlueJ');
insert into addonis_db.ides(ide_id, ide_name) values ('54937534-6e69-4ac7-a922-b5e2b355a6f0', 'Eclipse JDT');
insert into addonis_db.ides(ide_id, ide_name) values ('a175a3de-e048-40da-9819-ce14c5e6f37b', 'Geany');
insert into addonis_db.ides(ide_id, ide_name) values ('45541ede-f4d9-4b8d-891d-c1969058854e', 'Greenfoot');
insert into addonis_db.ides(ide_id, ide_name) values ('78815e19-9b25-4cff-9015-85bb87e42190', 'IntelliJ IDEA');
insert into addonis_db.ides(ide_id, ide_name) values ('425544cd-143a-4841-86eb-94a567dfa713', 'NetBeans');
insert into addonis_db.ides(ide_id, ide_name) values ('fcbbe431-cfb1-4ddf-b5e4-597f8cc5445d', 'Atom');
insert into addonis_db.ides(ide_id, ide_name) values ('d5935eee-865d-4140-b08f-c67040a57764', 'WebStorm');


insert into addonis_db.list_types(list_id, type) values ('4ddbdabf-be45-4db1-9b2d-026e0bea41a8', 'admin');